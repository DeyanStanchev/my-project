package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.exceptions.InvalidBrandInputException;
import com.telerikacademy.oop.cosmetics.models.exceptions.InvalidPriceInputException;
import com.telerikacademy.oop.cosmetics.models.exceptions.InvalidNameInputException;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

public class CreateProductCommand implements Command {

    private static final String PRODUCT_CREATED = "Product with name %s was created!";
    private static final String PRODUCT_EXISTS = "Product %s already exists!";
    private final static String INVALID_ARGUMENTS_COUNT = "CreateProduct command expects 4 parameters.";

    private static final int EXPECTED_ARGUMENTS_COUNT = 4;

    private final ProductRepository productRepository;

    public CreateProductCommand(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        try{
            ValidationHelpers.validateArgumentsCount(EXPECTED_ARGUMENTS_COUNT,parameters.size());
        }catch(IllegalArgumentException e){
            return INVALID_ARGUMENTS_COUNT;
        }

        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price;
        GenderType gender;
        try {
            price = ParsingHelpers.tryParseDouble(parameters.get(2));
        }catch(InvalidPriceInputException e){
            return "Input value for price must be a number!";
        }
        try{
            gender = ParsingHelpers.tryParseGender(parameters.get(3));
        }catch(IllegalArgumentException e){
            return "Gender type must be one of Men, Women or Unisex.";
        }

        return createProduct(name, brand, price, gender);
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        try{
            if(productRepository.productExist(name)){
                throw new IllegalArgumentException();
            }
        }catch(IllegalArgumentException e){
            return String.format(PRODUCT_EXISTS,name);
        }
        try {
            productRepository.createProduct(name, brand, price, gender);
        }catch(InvalidNameInputException e){
            return "Product name must be between 3 and 10 symbols.";
        }catch(InvalidPriceInputException e){
            return "Price can`t be negative!";
        }catch(InvalidBrandInputException e){
            return "Product brand must be between 2 and 10 symbols.";
        }
        return String.format(PRODUCT_CREATED, name);
    }
}

package com.telerikacademy.oop.cosmetics.utils;

import com.telerikacademy.oop.cosmetics.models.exceptions.InvalidBrandInputException;
import com.telerikacademy.oop.cosmetics.models.exceptions.InvalidPriceInputException;
import com.telerikacademy.oop.cosmetics.models.exceptions.InvalidNameInputException;

public class ValidationHelpers {

    public static void validateNameLength(String name, int minLength, int maxLength, String type) {
        if (name.length() < minLength || name.length() > maxLength) {
            if(type.equals("Name") || type.equals("Category")){
                throw new InvalidNameInputException();
            }else if(type.equals("Brand")){
                throw new InvalidBrandInputException();
            }
        }
    }


    public static void validatePrice(double price) {
        if (price < 0) {
            throw new InvalidPriceInputException();
        }
    }
    public static void validateArgumentsCount(int expectedCount,int count){
        if(expectedCount != count){
            throw new IllegalArgumentException();
        }
    }
}

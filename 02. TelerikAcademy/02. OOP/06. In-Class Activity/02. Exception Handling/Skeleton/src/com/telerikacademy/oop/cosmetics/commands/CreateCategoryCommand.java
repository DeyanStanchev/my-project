package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.models.exceptions.InvalidNameInputException;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

public class CreateCategoryCommand implements Command {

    private static final String CATEGORY_CREATED = "Category with name %s was created!";
    private static final String CATEGORY_EXISTS = "Category %s already exists!";
    private static final String INVALID_CATEGORY_NAME = "Category name must be between 3 and 10 symbols!";
    private static final String INVALID_ARGUMENTS_COUNT = "CreateCategory command expects 1 parameter.";
    private static final int EXPECTED_ARGUMENTS_COUNT = 1;

    private final ProductRepository productRepository;

    public CreateCategoryCommand(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        try{
            ValidationHelpers.validateArgumentsCount(EXPECTED_ARGUMENTS_COUNT,parameters.size());
        }catch(IllegalArgumentException e){
            return INVALID_ARGUMENTS_COUNT;
        }

        String categoryName = parameters.get(0);

        return createCategory(categoryName);
    }

    private String createCategory(String categoryName) {
        try{
            if(productRepository.categoryExist(categoryName)){
                throw new IllegalArgumentException();
            }
        }catch(IllegalArgumentException e){
            return String.format(CATEGORY_EXISTS,categoryName);
        }

        try{
            productRepository.createCategory(categoryName);
        }catch(InvalidNameInputException e){
            return INVALID_CATEGORY_NAME;
        }

        return String.format(CATEGORY_CREATED, categoryName);
    }

}

package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

public class ShowCategoryCommand implements Command {

    private static final int EXPECTED_ARGUMENTS_COUNT = 1;
    private static final String INVALID_ARGUMENTS_COUNT = "ShowCategory Command expects 1 parameter.";

    private final ProductRepository productRepository;

    public ShowCategoryCommand(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        try{
            ValidationHelpers.validateArgumentsCount(EXPECTED_ARGUMENTS_COUNT,parameters.size());
        }catch(IllegalArgumentException e){
            return INVALID_ARGUMENTS_COUNT;
        }

        String categoryName = parameters.get(0);

        return showCategory(categoryName);
    }

    private String showCategory(String categoryName) {
        Category category = productRepository.findCategoryByName(categoryName);

        return category.print();
    }

}

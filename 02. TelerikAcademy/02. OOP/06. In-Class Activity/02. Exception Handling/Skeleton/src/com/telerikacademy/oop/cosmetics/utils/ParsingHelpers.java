package com.telerikacademy.oop.cosmetics.utils;

import com.telerikacademy.oop.cosmetics.commands.CommandType;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.exceptions.InvalidNameInputException;
import com.telerikacademy.oop.cosmetics.models.exceptions.InvalidPriceInputException;

public class ParsingHelpers {

    public static double tryParseDouble(String number){
        try{
            return Double.parseDouble(number);
        }catch(NumberFormatException e){
            throw new InvalidPriceInputException();
        }
    }
    public static GenderType tryParseGender(String genderType){
        try{
            return GenderType.valueOf(genderType.toUpperCase());
        }catch(IllegalArgumentException e){
            throw new IllegalArgumentException();
        }
    }
    public static CommandType tryParseCommand(String commandType){
        try{
            return CommandType.valueOf(commandType.toUpperCase());
        }catch(IllegalArgumentException e){
            throw new IllegalArgumentException();
        }
    }
}

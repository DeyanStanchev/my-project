package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand.EXPECTED_PARAMETERS_COUNT;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Category.CATEGORY_VALID_NAME_LENGTH;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Product.*;

public class AddProductToCategoryCommandTests {
    private Command command;
    private ProductRepository repository;

    @BeforeEach
    public void before() {
        this.repository = new ProductRepositoryImpl();
        this.command = new AddProductToCategoryCommand(repository);
    }

    @Test
    public void execute_should_addProduct_when_validParameters(){
        repository.createCategory(TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));
        repository.createProduct(TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN
        );
        command.execute(List.of(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH)));

        Assertions.assertEquals(1,repository.getProducts().size());
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        List<String> arguments = TestUtilities.initializeListWithSize(EXPECTED_PARAMETERS_COUNT - 1);

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_missingParameters() {
        List<String> arguments = TestUtilities.initializeListWithSize(EXPECTED_PARAMETERS_COUNT);

        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> arguments.get(2));
    }

    @Test
    public void execute_should_throwException_when_productDoesNotExist(){
        repository.createCategory(TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));
        Assertions.assertThrows(InvalidUserInputException.class,() -> command.execute(List.of(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH),
                "a")));
    }

    @Test
    public void execute_should_throwException_when_categoryDoesNotExist(){
        repository.createProduct(TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN
        );

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(
                "a",
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH))));
    }
}

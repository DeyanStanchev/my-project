package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;


import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Category.*;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Product.*;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.initializeStringWithSize;

import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class CategoryImplTests {

    @Test
    public void constructor_should_createCategory_when_argumentsAreValid() {

        Assertions.assertDoesNotThrow(() -> new CategoryImpl(initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH)));

    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {CATEGORY_MIN_NAME_LENGTH - 1, CATEGORY_MAX_NAME_LENGTH + 1})
    public void constructor_should_throwException_when_nameShorterThanExpected(int invalidName) {
        Assertions.assertThrows(InvalidUserInputException.class, () -> new CategoryImpl(initializeStringWithSize(invalidName)));
    }

    @Test
    public void addProduct_should_addProductToList() {
        Category category = new CategoryImpl(initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));
        category.addProduct(new ProductImpl(
                initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN
        ));
        Assertions.assertEquals(1,category.getProducts().size());
    }

    @Test
    public void removeProduct_should_removeProductFromList_when_pProductExist() {
        Category category = new CategoryImpl(initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));
        Product product = new ProductImpl(
                initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN
        );
        category.addProduct(product);
        category.removeProduct(product);
        Assertions.assertEquals(0, category.getProducts().size());
    }

    @Test
    public void removeProduct_should_notRemoveProductFromList_when_productNotExist() {
        Category category = new CategoryImpl(initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));
        Product product = new ProductImpl(
                initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN
        );
        category.addProduct(product);
        category.removeProduct(new ProductImpl(
                initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH + 1),
                initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH + 1),
                PRODUCT_VALID_PRICE,
                GenderType.MEN
        ));
        Assertions.assertEquals(1, category.getProducts().size());
    }
}

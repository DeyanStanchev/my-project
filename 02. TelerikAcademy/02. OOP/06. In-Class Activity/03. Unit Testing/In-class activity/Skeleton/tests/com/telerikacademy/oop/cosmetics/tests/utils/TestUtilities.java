package com.telerikacademy.oop.cosmetics.tests.utils;

import java.util.Arrays;
import java.util.List;

public class TestUtilities {

    public static String initializeStringWithSize(int wantedSize) {
        return "x".repeat(wantedSize);
    }
    public static List<String> initializeListWithSize(int wantedSize) {
        return Arrays.asList(new String[wantedSize]);
    }
}


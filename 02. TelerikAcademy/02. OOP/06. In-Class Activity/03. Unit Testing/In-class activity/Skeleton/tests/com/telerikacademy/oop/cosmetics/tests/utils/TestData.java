package com.telerikacademy.oop.cosmetics.tests.utils;

import org.junit.jupiter.api.Test;


import static com.telerikacademy.oop.cosmetics.models.CategoryImpl.NAME_MIN_LENGTH;
import static com.telerikacademy.oop.cosmetics.models.CategoryImpl.NAME_MAX_LENGTH;
import static com.telerikacademy.oop.cosmetics.models.ProductImpl.BRAND_MIN_LENGTH;
import static com.telerikacademy.oop.cosmetics.models.ProductImpl.BRAND_MAX_LENGTH;


public class TestData {
    public static class Category {
        public static final int CATEGORY_MIN_NAME_LENGTH = NAME_MIN_LENGTH;
        public static final int CATEGORY_MAX_NAME_LENGTH = NAME_MAX_LENGTH;
        public static final int CATEGORY_VALID_NAME_LENGTH = NAME_MIN_LENGTH + 1;
    }

    public static class Product {
        public static final int PRODUCT_NAME_MIN_LENGTH = NAME_MIN_LENGTH;
        public static final int PRODUCT_NAME_MAX_LENGTH = NAME_MAX_LENGTH;
        public static final int PRODUCT_VALID_NAME_LENGTH = NAME_MIN_LENGTH + 1;
        public static final int PRODUCT_BRAND_MIN_LENGTH = BRAND_MIN_LENGTH;
        public static final int PRODUCT_BRAND_MAX_LENGTH = BRAND_MAX_LENGTH;
        public static final int PRODUCT_BRAND_VALID_NAME_LENGTH = BRAND_MIN_LENGTH + 1;
        public static final double PRODUCT_VALID_PRICE = 2.22;
        public static final double PRODUCT_INVALID_PRICE = -1.0;
    }
}

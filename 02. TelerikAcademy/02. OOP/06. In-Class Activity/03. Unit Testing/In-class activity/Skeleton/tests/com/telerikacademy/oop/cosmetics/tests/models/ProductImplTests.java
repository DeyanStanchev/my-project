package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Product.*;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities.initializeStringWithSize;

public class ProductImplTests {
    @Test
    public void constructor_should_createCategory_when_argumentsAreValid() {

        Assertions.assertDoesNotThrow(()->new ProductImpl(
                initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN ));
    }

    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {PRODUCT_NAME_MIN_LENGTH - 1, PRODUCT_NAME_MAX_LENGTH + 1})
    public void should_throwException_when_nameLengthOutOfBounds(int testLength){

        Assertions.assertThrows(InvalidUserInputException.class, () ->new ProductImpl(
                initializeStringWithSize(testLength),
                initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN
        ));
    }
    @ParameterizedTest(name = "with length {0}")
    @ValueSource(ints = {PRODUCT_BRAND_MIN_LENGTH - 1, PRODUCT_BRAND_MAX_LENGTH + 1})
    public void should_throwException_when_brandNameLengthOutOfBounds(int testLength){

        Assertions.assertThrows(InvalidUserInputException.class, () ->new ProductImpl(
                initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                initializeStringWithSize(testLength),
                PRODUCT_VALID_PRICE,
                GenderType.MEN
        ));
    }

    @Test
    public void should_throwException_when_negativePrice(){
        Assertions.assertThrows(InvalidUserInputException.class, () ->new ProductImpl(
                initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_INVALID_PRICE,
                GenderType.MEN
        ));
    }
    @Test
    public void should_throwException_when_genderTypeNotValid(){
        Assertions.assertThrows(IllegalArgumentException.class, () ->new ProductImpl(
                initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.valueOf("Boy")
        ));
    }
}

package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.CreateProductCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.commands.CreateProductCommand.EXPECTED_PARAMETERS_COUNT;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Product.*;

public class CreateProductCommandTests {

    private Command command;
    private ProductRepository repository;

    @BeforeEach
    public void before() {
        this.repository = new ProductRepositoryImpl();
        this.command = new CreateProductCommand(repository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        List<String> arguments = TestUtilities.initializeListWithSize(EXPECTED_PARAMETERS_COUNT - 1);

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_addNewProductToRepository_when_validParameters() {
        command.execute(List.of(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                String.valueOf(PRODUCT_VALID_PRICE),
                GenderType.MEN.toString()
        ));

        Assertions.assertEquals(1, repository.getProducts().size());
    }

    @Test
    public void execute_should_throwException_when_missingParameters() {

        List<String> arguments = TestUtilities.initializeListWithSize(EXPECTED_PARAMETERS_COUNT);


        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> arguments.get(4));

    }

    @Test
    public void execute_should_throwException_when_priceIsNotANumber() {

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                "а",
                GenderType.MEN.toString()
        )));
    }
    @Test
    public void execute_should_throwException_when_genderTypeNotValid(){
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                String.valueOf(PRODUCT_VALID_PRICE),
                "Boy"
        )));
    }

    @Test
    public void execute_should_throwException_when_duplicateCategoryName() {
        command.execute(List.of(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                String.valueOf(PRODUCT_VALID_PRICE),
                GenderType.MEN.toString()
        ));

        Assertions.assertThrows(DuplicateEntityException.class, () -> command.execute(List.of(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                String.valueOf(PRODUCT_VALID_PRICE),
                GenderType.MEN.toString()
        )));
    }

}


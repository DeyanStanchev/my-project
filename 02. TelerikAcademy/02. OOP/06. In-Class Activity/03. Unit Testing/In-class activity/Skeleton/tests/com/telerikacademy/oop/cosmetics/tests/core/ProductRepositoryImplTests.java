package com.telerikacademy.oop.cosmetics.tests.core;

import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Category.CATEGORY_VALID_NAME_LENGTH;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Product.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductRepositoryImplTests {
    ProductRepository repository;

    @BeforeEach
    public void before() {
        repository = new ProductRepositoryImpl();
    }

    @Test
    public void constructor_should_initializeAllCollections() {

        Assertions.assertAll(
                () -> assertEquals(0, repository.getProducts().size()),
                () -> assertEquals(0, repository.getCategories().size())
        );
    }

    @Test
    public void getCategories_should_returnCopyOfCollection() {
        repository.createCategory(TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));
        List<Category> toCheck = repository.getCategories();

        Assertions.assertEquals(toCheck, repository.getCategories());

    }

    @Test
    public void getProducts_should_returnCopyOfCollection() {
        repository.createProduct(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN);
        List<Product> toCheck = repository.getProducts();

        Assertions.assertEquals(toCheck, repository.getProducts());
    }

    @Test
    public void categoryExists_should_returnTrue_whenCategoryExists() {
        repository.createCategory(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));

        Assertions.assertTrue(repository.categoryExist(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH)
        ));
    }
    @Test
    public void categoryExists_should_returnFalse_whenCategoryDoesNotExists() {
        repository.createCategory(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));

        Assertions.assertFalse(repository.categoryExist(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH+1)
        ));
    }


    @Test
    public void productExists_should_returnTrue_whenProductExists() {
        repository.createProduct(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN);

        Assertions.assertTrue(repository.productExist(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH)
        ));
    }
    @Test
    public void productExists_should_returnFalse_whenProductDoesNotExists() {
        repository.createProduct(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN);

        Assertions.assertFalse(repository.productExist(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH+1)
        ));
    }

    @Test
    public void createProduct_should_createSuccessfully_whenArgumentsAreValid() {
        Assertions.assertDoesNotThrow(() -> repository.createProduct(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN));
    }

    @Test
    public void createCategory_should_createSuccessfully_whenArgumentsAreValid() {
        Assertions.assertDoesNotThrow(() -> repository.createCategory(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH)));

    }

    @Test
    public void findCategoryByName_should_returnCategory_ifExists() {
        repository.createCategory(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));

        Assertions.assertAll(
                () -> assertDoesNotThrow(() -> repository.findCategoryByName(
                        TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH))),
                () -> assertEquals(
                        TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH),
                        repository.findCategoryByName(
                                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH)).getName()));
    }

    @Test
    public void findCategoryByName_should_throwException_ifDoesNotExist() {
        repository.createCategory(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH+1));

        Assertions.assertThrows(InvalidUserInputException.class, () -> repository.findCategoryByName(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH)));

    }

    @Test
    public void findProductByName_should_returnProduct_ifExists() {
        repository.createProduct(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN);

        Assertions.assertAll(
                () -> assertDoesNotThrow(() -> repository.findProductByName(
                        TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH))),
                () -> assertEquals(
                        TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH),
                        repository.findProductByName(
                                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH)).getName()));
    }

    @Test
    public void findProductByName_should_throwException_ifDoesNotExist() {
        repository.createProduct(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH+1),
                TestUtilities.initializeStringWithSize(PRODUCT_BRAND_VALID_NAME_LENGTH),
                PRODUCT_VALID_PRICE,
                GenderType.MEN);
        Assertions.assertThrows(InvalidUserInputException.class, () -> repository.findProductByName(
                TestUtilities.initializeStringWithSize(PRODUCT_VALID_NAME_LENGTH)));
    }

}

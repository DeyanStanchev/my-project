package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.ShowCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.cosmetics.commands.ShowCategoryCommand.EXPECTED_PARAMETERS_COUNT;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Category.CATEGORY_VALID_NAME_LENGTH;


public class ShowCategoryCommandTests {
    private Command command;
    private ProductRepository repository;

    @BeforeEach
    public void before() {
        this.repository = new ProductRepositoryImpl();
        this.command = new ShowCategoryCommand(repository);
    }

    @Test
    public void execute_should_showCategory_when_validParameters() {
        repository.createCategory(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));

        Assertions.assertDoesNotThrow(() -> command.execute(List.of(
                TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH))));
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        List<String> arguments = TestUtilities.initializeListWithSize(EXPECTED_PARAMETERS_COUNT - 1);

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_missingParameters() {
        List<String> arguments = TestUtilities.initializeListWithSize(ShowCategoryCommand.EXPECTED_PARAMETERS_COUNT);

        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> arguments.get(1));
    }

    @Test
    public void execute_should_throwException_when_categoryDoesNotExist(){
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(List.of(
                    "a")));
        }
    }


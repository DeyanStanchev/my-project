package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateCategoryCommand;
import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.cosmetics.commands.CreateCategoryCommand.EXPECTED_PARAMETERS_COUNT;
import static com.telerikacademy.oop.cosmetics.tests.utils.TestData.Category.CATEGORY_VALID_NAME_LENGTH;

public class CreateCategoryCommandTests {
    private Command command;
    private ProductRepository repository;

    @BeforeEach
    public void before() {
        this.repository = new ProductRepositoryImpl();
        this.command = new CreateCategoryCommand(repository);
    }
    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        List<String> arguments = TestUtilities.initializeListWithSize(EXPECTED_PARAMETERS_COUNT - 1);

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_addNewCategoryToRepository_when_validParameters() {
            List<String> parameters = List.of(TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH));

            Assertions.assertDoesNotThrow(()->command.execute(parameters));
    }

    @Test
    public void execute_should_throwException_when_missingParameters() {
        List<String> arguments = TestUtilities.initializeListWithSize(0);

        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_duplicateCategoryName() {
            command.execute(List.of(TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH)));

            Assertions.assertThrows(DuplicateEntityException.class, () ->command.execute(List.of(
                    TestUtilities.initializeStringWithSize(CATEGORY_VALID_NAME_LENGTH))));
    }

}

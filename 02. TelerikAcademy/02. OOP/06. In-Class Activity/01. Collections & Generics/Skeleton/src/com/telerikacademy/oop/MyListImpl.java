package com.telerikacademy.oop;

import java.util.Arrays;
import java.util.Iterator;

public class MyListImpl<T> implements MyList<T> {

    private static final int INITIAL_CAPACITY = 4;

    private T[] array;
    private int size;


    public MyListImpl() {
        array = (T[]) new Object[INITIAL_CAPACITY];
        size = 0;
    }

    public MyListImpl(int passedSize) {
        if (passedSize <= 0) {
            throw new IllegalArgumentException("Invalid size: " + passedSize);
        }
        array = (T[]) new Object[passedSize];
        size = 0;
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void add(T element) {
        if (size == capacity()) {
            doubleArraySize();
        }
        array[size++] = element;
    }

    private void doubleArraySize() {
        T[] tmpArray = (T[]) new Object[size * 2];
        System.arraycopy(array, 0, tmpArray, 0, array.length);
        array = tmpArray;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index > size - 1) {
            throw new ArrayIndexOutOfBoundsException("Invalid index: " + index);
        }
        return array[index];
    }

    @Override
    public int indexOf(T element) {
        for (int i = 0; i < size; i++) {
            if (element.equals(array[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(T element) {
        for (int i = size - 1; i >= 0; i--) {
            if (element.equals(array[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) >= 0;
    }

    @Override
    public void removeAt(int index) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException("Invalid index: " + index);
        }
        array[index] = null;
        for (int i = index; i < size - 1; i++) {
            array[index] = array[index + 1];
            array[index + 1] = null;
        }
        size--;
    }

    @Override
    public boolean remove(T element) {
        int indexFoundAt = -1;
        for (int i = 0; i < size; i++) {
            if (element.equals(array[i])) {
                array[i] = null;
                indexFoundAt = i;
                break;
            }
        }
        if (indexFoundAt != -1) {
            for (int i = indexFoundAt; i < size() - 1; i++) {
                array[i] = array[i + 1];
                array[i + 1] = null;
            }
            size--;
            return true;
        }
        return false;
    }


    @Override
    public void clear() {
        Arrays.fill(array, null);
        size = 0;
    }

    @Override
    public void swap(int from, int to) {
        T tmp = array[from];
        array[from] = array[to];
        array[to] = tmp;
    }

    @Override
    public void print() {
        if (isEmpty()) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            if (array[i] != null) {
                if (i != size - 1) {
                    sb.append(String.format("%s, ", array[i]));
                } else {
                    sb.append(String.format("%s", array[i]));
                }
            }
        }
        sb.append("]");
        System.out.println(sb.toString());
    }

    @Override
    public Iterator<T> iterator() {
        return new MyListIterator();
    }

    private class MyListIterator implements Iterator<T> {
        private int currentIndex;

        MyListIterator() {
            currentIndex = 0;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public T next() {
            T currentElement = array[currentIndex];
            currentIndex++;
            return currentElement;
        }
    }
}

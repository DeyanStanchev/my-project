package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Comment;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.models.enums.VehicleType;
import com.telerikacademy.oop.dealership.utils.FormattingHelpers;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;


import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.dealership.models.ModelConstants.*;

public abstract class VehicleBase implements Vehicle {

    private static final String VEHICLE_PRINT_FORMAT = ". %s:%n" +
                                                       "Make: %s%n" +
                                                       "Model: %s%n" +
                                                       "Wheels: %d%n" +
                                                       "Price: $%.0f%n";

    private String make;
    private String model;
    private double price;
    private final int wheels;
    private final VehicleType vehicleType;
    private final List<Comment> comments;

    public VehicleBase(String make, String model, double price, VehicleType vehicleType, int wheels) {
        setMake(make);
        setModel(model);
        setPrice(price);
        this.wheels = wheels;
        this.vehicleType = vehicleType;
        comments = new ArrayList<>();

    }

    private void setMake(String make) {
        ValidationHelpers.validateStringRange(make, MAKE_NAME_LEN_MIN, MAKE_NAME_LEN_MAX, MAKE_NAME_LEN_ERR);
        this.make = make;
    }

    private void setModel(String model) {
        ValidationHelpers.validateStringRange(model, MODEL_NAME_LEN_MIN, MODEL_NAME_LEN_MAX, MODEL_NAME_LEN_ERR);
        this.model = model;
    }

    private void setPrice(double price) {
        ValidationHelpers.validateDecimalRange(price, PRICE_VAL_MIN, PRICE_VAL_MAX, PRICE_VAL_ERR);
        FormattingHelpers.removeTrailingZerosFromDouble(price);
        this.price = price;
    }

    @Override
    public int getWheels() {
        return wheels;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return String.format(VEHICLE_PRINT_FORMAT,
                getType(),
                getMake(),
                getModel(),
                getWheels(),
                getPrice());
    }

    protected String printComments() {
        if (getComments().isEmpty()) {
            return NO_COMMENTS_HEADER;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(COMMENTS_HEADER);
        for (Comment comment : comments) {
            sb.append(comment.toString());
        }
        sb.append(COMMENTS_FOOTER);
        return sb.toString();
    }
}

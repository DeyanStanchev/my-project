package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.core.contracts.VehicleDealershipRepository;
import com.telerikacademy.oop.dealership.models.contracts.User;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import java.util.List;

public class ShowUsersCommand extends BaseCommand{

    private static final String NOT_AN_ADMIN_MESSAGE = "You are not an admin!";
    private final static String USERS_HEADER = "--USERS--\n";

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    public ShowUsersCommand(VehicleDealershipRepository vehicleDealershipRepository) {
        super(vehicleDealershipRepository);
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters,EXPECTED_NUMBER_OF_ARGUMENTS);

        if (getVehicleDealershipRepository().getLoggedInUser().isAdmin()) {
            List<User> users = getVehicleDealershipRepository().getUsers();
            StringBuilder sb = new StringBuilder();
            int counter =1;
            sb.append(USERS_HEADER);
            for (User user : users) {
                if (counter < users.size()) {
                    sb.append(counter++).append(user.toString()).append("\n");
                }else{
                    sb.append(counter).append(user.toString());
                }
            }
            return sb.toString();
        }
        return NOT_AN_ADMIN_MESSAGE;
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}

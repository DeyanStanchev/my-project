package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Truck;
import com.telerikacademy.oop.dealership.models.enums.VehicleType;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import static java.lang.String.format;

public class TruckImpl extends VehicleBase implements Truck {

    public static final int WEIGHT_CAP_MIN = 1;
    public static final int WEIGHT_CAP_MAX = 100;
    private static final String WEIGHT_CAP_ERR = format(
            "Weight capacity must be between %d and %d!",
            WEIGHT_CAP_MIN,
            WEIGHT_CAP_MAX);
    private static final String TRUCK_PRINT_FORMAT = "%s" +
                                                     "Weight Capacity: %dt%n" +
                                                     "%s";
    private static final int WHEELS = 8;

    private int weightCapacity;


    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK,WHEELS);
        setWeightCapacity(weightCapacity);

    }

    private void setWeightCapacity(int weightCapacity) {
        ValidationHelpers.validateIntRange(weightCapacity,WEIGHT_CAP_MIN,WEIGHT_CAP_MAX,WEIGHT_CAP_ERR);
        this.weightCapacity = weightCapacity;
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    @Override
    public String toString(){
        return String.format(TRUCK_PRINT_FORMAT,
                super.toString(),
                getWeightCapacity(),
                printComments());
    }
}

package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Motorcycle;
import com.telerikacademy.oop.dealership.models.enums.VehicleType;
import com.telerikacademy.oop.dealership.utils.ValidationHelpers;

import static java.lang.String.format;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    public static final int CATEGORY_LEN_MIN = 3;
    public static final int CATEGORY_LEN_MAX = 10;
    private static final String CATEGORY_LEN_ERR = format(
            "Category must be between %d and %d characters long!",
            CATEGORY_LEN_MIN,
            CATEGORY_LEN_MAX);
    private static final String MOTORCYCLE_PRINT_FORMAT = "%s" +
                                                          "Category: %s%n" +
                                                          "%s";
    private static final int WHEELS = 2;

    private String category;


    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE,WHEELS);
        setCategory(category);
    }

    private void setCategory(String category) {
        ValidationHelpers.validateStringRange(category,CATEGORY_LEN_MIN,CATEGORY_LEN_MAX,CATEGORY_LEN_ERR);
        this.category = category;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public String toString(){
        return String.format(MOTORCYCLE_PRINT_FORMAT,
                super.toString(),
                getCategory(),
                printComments());
    }
}

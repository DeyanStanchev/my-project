package com.telerikacademy.oop.cosmetics.models;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<Product> products;

    public ShoppingCart() {
        products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void removeProduct(Product product) {
        if(!containsProduct(product)){
            throw new IllegalArgumentException("Product was not found");
        }
        products.remove(product);
    }
    
    public boolean containsProduct(Product product) {
        return products.contains(product);
    }
    
    public double totalPrice() {
        double sum = 0.00;
        for (Product product : products) {
            sum += product.getPrice();
        }
        return sum;
    }
}

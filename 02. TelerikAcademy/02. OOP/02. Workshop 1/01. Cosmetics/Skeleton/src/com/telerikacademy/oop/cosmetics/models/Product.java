package com.telerikacademy.oop.cosmetics.models;

import java.util.Objects;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

public class Product {

    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final int BRAND_MIN_LENGTH = 2;
    public static final int BRAND_MAX_LENGTH = 10;

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {
        setName((name));
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(name,NAME_MIN_LENGTH,NAME_MAX_LENGTH,"Product name must be between 3 and 10 symbols.");
        this.name=name;
    }

    public String getName() {
        return this.name;
    }

    private void setBrand(String brand) {
        ValidationHelpers.validateStringLength(brand,BRAND_MIN_LENGTH,BRAND_MAX_LENGTH,"Brand name must be between 2 and 10 symbols.");
        this.brand = brand;
    }

    public String getBrand() {
        return this.brand;
    }

    private void setPrice(double price) {
        if(price<0.00){
            throw new IllegalArgumentException("Price can`t be negative number.");
        }
        this.price=price;
    }

    public double getPrice() {
        return this.price;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    public GenderType getGender() {
        return this.gender;
    }

    public String print() {
        return String.format("#Name %s%n" +
                             "#Price: %.2f%n" +
                             "#Gender: %s%n" +
                             "===%n",this.getName(),this.getPrice(),this.getGender());
    }

    @Override
    public boolean equals(Object p) {
        if (this == p) return true;
        if (p == null || getClass() != p.getClass()) return false;
        Product product = (Product) p;
        return Double.compare(this.getPrice(), product.getPrice()) == 0 &&
                Objects.equals(this.getName(), product.getName()) &&
                Objects.equals(this.getBrand(), product.getBrand()) &&
                this.getGender() == product.getGender();
    }
}

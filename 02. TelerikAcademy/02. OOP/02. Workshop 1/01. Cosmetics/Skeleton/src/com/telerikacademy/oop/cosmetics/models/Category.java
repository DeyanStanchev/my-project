package com.telerikacademy.oop.cosmetics.models;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

import com.telerikacademy.oop.cosmetics.core.CosmeticsRepositoryImpl;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

public class Category {
    public static final int NAME_MIN_LENGTH = 2;
    public static final int NAME_MAX_LENGTH = 15;
    private String name;
    private final List<Product> products;

    public Category(String name) {
        setName(name);
        products = new ArrayList<>();
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(name, NAME_MIN_LENGTH, NAME_MAX_LENGTH, "Category name must be between 2 and 15 symbols.");
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void removeProduct(Product product) {
        if (!products.contains(product)) {
            throw new IllegalArgumentException("Product was not found.");
        } else {
            products.remove(product);
        }
    }

    public String print() {
        if (products.isEmpty()) {
            return String.format("#Category: %s%n#No products in this category%n", name);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Category: %s%n",name));
        for (Product product : products) {
            sb.append(product.print());
        }
        return sb.toString();
    }
}

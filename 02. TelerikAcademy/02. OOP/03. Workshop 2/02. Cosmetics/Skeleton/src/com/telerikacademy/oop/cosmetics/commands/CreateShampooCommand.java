package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.ShampooImpl;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.UsageType;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.math.BigInteger;
import java.util.List;

public class CreateShampooCommand implements Command {


    private static final String SHAMPOO_CREATED = "Shampoo with name %s was created!";
    private static final String SHAMPOO_ALREADY_EXIST = "Shampoo with name %s already exists!";
    private static final String INVALID_PRICE = "Invalid value for price. Should be a number.";
    private static final String INVALID_SIZE = "Invalid size of product. Shampoo should not be empty.";

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 6;

    private final CosmeticsRepository cosmeticsRepository;

    public CreateShampooCommand(CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsRepository = cosmeticsRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters,EXPECTED_NUMBER_OF_ARGUMENTS);
        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price = ParsingHelpers.tryParseDouble(parameters.get(2),INVALID_PRICE);
        GenderType genderType = ParsingHelpers.tryParseGender(parameters.get(3).toUpperCase());
        int milliliters = ParsingHelpers.tryParseInt(parameters.get(4),INVALID_SIZE );
        UsageType usageType = ParsingHelpers.tryParseUsageType(parameters.get(5).toUpperCase());
        return createShampoo(name,brand,price,genderType,milliliters,usageType);
    }


    private String createShampoo(String name, String brand, double price, GenderType genderType, int milliliters, UsageType usageType) {
        if (cosmeticsRepository.productExist(name)) {
            throw new IllegalArgumentException(String.format(SHAMPOO_ALREADY_EXIST, name));
        }

        cosmeticsRepository.createShampoo(name, brand, price, genderType,milliliters,usageType);

        return String.format(SHAMPOO_CREATED, name);
    }
}

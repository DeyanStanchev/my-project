package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.contracts.Cream;
import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.ScentType;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

public class CreamImpl extends ProductImpl implements Cream {
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 15;
    public static final int BRAND_NAME_MIN_LENGTH = 3;
    public static final int BRAND_NAME_MAX_LENGTH = 15;

    private ScentType scentType;


    public CreamImpl(String name, String brandName, double price, GenderType genderType, ScentType scentType){
        super(name,brandName,price,genderType);
        setScentType(scentType);
    }

    @Override
    protected void validateName(String name) {
        ValidationHelpers.validateStringLength(name,NAME_MIN_LENGTH,NAME_MAX_LENGTH,"Name");
    }

    @Override
    protected void validateBrand(String brand) {
        ValidationHelpers.validateStringLength(brand,BRAND_NAME_MIN_LENGTH,BRAND_NAME_MAX_LENGTH,"Brand");
    }



    private void setScentType(ScentType scentType) {
        this.scentType = scentType;
    }


    @Override
    public ScentType getScentType() {
        return scentType;
    }



    @Override
    public String print() {
        return String.format("#%s %s%n" +
                             " #Price: %s%n" +
                             " #Gender: %s%n" +
                             " #Scent: %s%n",getName(),getBrandName(),getPrice(),getGenderType(),getScentType());
    }
}

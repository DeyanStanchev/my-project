package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.UsageType;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class CreateToothpasteCommand implements Command {


    private static final String TOOTHPASTE_CREATED = "Toothpaste with name %s was created!";
    private static final String TOOTHPASTE_ALREADY_EXIST = "Toothpaste with name %s already exists!";
    private static final String INVALID_PRICE = "Invalid value for price. Should be a number.";

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;

    private final CosmeticsRepository cosmeticsRepository;

    public CreateToothpasteCommand(CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsRepository = cosmeticsRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters,EXPECTED_NUMBER_OF_ARGUMENTS);
        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price = ParsingHelpers.tryParseDouble(parameters.get(2),INVALID_PRICE);
        GenderType genderType = ParsingHelpers.tryParseGender(parameters.get(3).toUpperCase());
        List<String> ingredients = fillIngredients(parameters);

        return createToothpaste(name,brand,price,genderType,ingredients);
    }

    private ArrayList<String> fillIngredients(List<String> parameters){
        ArrayList<String> ingredients = new ArrayList<>();
        for (int i = 4; i <parameters.size() ; i++) {
            ingredients.add(parameters.get(i));
        }
        return ingredients;
    }

    private String createToothpaste(String name, String brand, double price, GenderType genderType, List<String> ingredients) {
        if (cosmeticsRepository.productExist(name)) {
            throw new IllegalArgumentException(String.format(TOOTHPASTE_ALREADY_EXIST, name));
        }

        cosmeticsRepository.createToothpaste(name, brand, price, genderType,ingredients);

        return String.format(TOOTHPASTE_CREATED, name);
    }

}
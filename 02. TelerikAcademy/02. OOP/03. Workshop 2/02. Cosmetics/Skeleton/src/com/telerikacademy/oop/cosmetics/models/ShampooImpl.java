package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.contracts.Shampoo;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.UsageType;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

public class ShampooImpl extends ProductImpl implements Shampoo {

    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;
    public static final int BRAND_NAME_MIN_LENGTH = 2;
    public static final int BRAND_NAME_MAX_LENGTH = 10;

    private int milliliters;
    private UsageType usageType;


    public ShampooImpl(String name, String brand, double price, GenderType genderType, int milliliters, UsageType usageType) {
        super(name,brand,price,genderType);
        setMilliliters(milliliters);
        setUsageType(usageType);
    }


    private void setMilliliters(int milliliters) {
        if(milliliters<0){
            throw new IllegalArgumentException("Milliliters can`t be negative.");
        }
        this.milliliters = milliliters;
    }

    private void setUsageType(UsageType usageType) {
        this.usageType = usageType;
    }

    @Override
    public int getMillilitres() {
        return milliliters;
    }

    @Override
    public UsageType getUsageType() {
        return usageType;
    }

    @Override
    protected void validateName(String name) {
        ValidationHelpers.validateStringLength(name,NAME_MIN_LENGTH,NAME_MAX_LENGTH,"Name");
    }

    @Override
    protected void validateBrand(String brand) {
        ValidationHelpers.validateStringLength(brand,BRAND_NAME_MIN_LENGTH,BRAND_NAME_MAX_LENGTH,"Brand");
    }

    @Override
    public String print() {
        return String.format("#%s %s%n"+
                             " #Price: $%.2f%n" +
                             " #Gender: %s%n" +
                             " #Milliliters: %d%n" +
                             " #Usage: %s%n",getName(),getBrandName(),getPrice(),getGenderType(),getMillilitres(),getUsageType());
    }

     //This method should be uncommented when you are done with the class.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShampooImpl shampoo = (ShampooImpl) o;
        return getName().equals(shampoo.getName()) &&
                getBrandName().equals(shampoo.getBrandName()) &&
                getPrice() == shampoo.getPrice() &&
                getGenderType().equals(shampoo.getGenderType()) &&
                getMillilitres() == shampoo.getMillilitres() &&
                getUsageType().equals(shampoo.getUsageType());
    }
}

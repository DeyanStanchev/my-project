package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.contracts.Product;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

public abstract class ProductImpl implements Product {

    private final static String INVALID_PRICE = "Price can`t be negative.";

    private String name;
    private String brand;
    private double price;
    private GenderType genderType;

    public ProductImpl(String name, String brand, double price, GenderType genderType) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGenderType(genderType);
    }

    protected abstract void validateName(String name);

    protected abstract void validateBrand(String brand);


    private void setName(String name) {
        validateName(name);
        this.name = name;
    }

    private void setBrand(String brand) {
        validateBrand(brand);
        this.brand = brand;
    }




    private void setPrice(double price) {
        if(price<0.00){
            throw new IllegalArgumentException(INVALID_PRICE);
        }
        this.price = price;
    }


    private void setGenderType(GenderType genderType) {
        this.genderType = genderType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrandName() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGenderType() {
        return genderType;
    }

    @Override
    public abstract String print();

}

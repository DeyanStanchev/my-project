package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.enums.GenderType;
import com.telerikacademy.oop.cosmetics.models.enums.ScentType;
import com.telerikacademy.oop.cosmetics.models.enums.UsageType;
import com.telerikacademy.oop.cosmetics.utils.ParsingHelpers;
import com.telerikacademy.oop.cosmetics.utils.ValidationHelpers;

import java.util.List;

public class CreateCreamCommand implements Command {


    private static final String CREAM_CREATED = "Cream with name %s was created!";
    private static final String CREAM_ALREADY_EXIST = "Cream with name %s already exists!";
    private static final String INVALID_PRICE = "Invalid value for price. Should be a number.";

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;

    private final CosmeticsRepository cosmeticsRepository;

    public CreateCreamCommand(CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsRepository = cosmeticsRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters,EXPECTED_NUMBER_OF_ARGUMENTS);
        String name = parameters.get(0);
        String brand = parameters.get(1);
        double price = ParsingHelpers.tryParseDouble(parameters.get(2),INVALID_PRICE);
        GenderType genderType = ParsingHelpers.tryParseGender(parameters.get(3).toUpperCase());
        ScentType scentType = ParsingHelpers.tryParseScent(parameters.get(4).toUpperCase());
        return createCream(name,brand,price,genderType,scentType);
    }

    private String createCream(String name, String brand, double price, GenderType genderType,ScentType scentType) {
        if (cosmeticsRepository.productExist(name)) {
            throw new IllegalArgumentException(String.format(CREAM_ALREADY_EXIST, name));
        }

        cosmeticsRepository.createCream(name, brand, price, genderType,scentType);

        return String.format(CREAM_CREATED, name);
    }

}

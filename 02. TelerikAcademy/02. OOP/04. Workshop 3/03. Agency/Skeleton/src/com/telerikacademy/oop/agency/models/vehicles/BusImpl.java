package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.vehicles.contracts.Bus;
import com.telerikacademy.oop.agency.models.vehicles.enums.VehicleType;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public class BusImpl extends VehicleBase implements Bus {

    public static final String INVALID_PASSENGER_CAPACITY = "A bus cannot have less than 10 passengers or more than 50 passengers." ;
    public static final String INVALID_PRICE = "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!" ;
    public static final int PASSENGER_MIN_VALUE = 10;
    public static final int PASSENGER_MAX_VALUE = 50;
    public static final double PRICE_MIN_VALUE = 0.1;
    public static final double PRICE_MAX_VALUE = 2.5;

    private static final VehicleType vehicleType = VehicleType.LAND;


    public BusImpl(int id, int passengerCapacity, double pricePerKilometer) {
        super(id,passengerCapacity,pricePerKilometer,vehicleType);
    }


    @Override
    public String getAsString() {
       return String.format("Bus %s",
               super.getAsString());
    }


    @Override
    void validatePassengersCount(int passengerCapacity) {
        ValidationHelper.validateValueInRange(passengerCapacity,
                                              PASSENGER_MIN_VALUE,
                                              PASSENGER_MAX_VALUE,
                                              INVALID_PASSENGER_CAPACITY);
    }

    @Override
    void validatePricePerKm(double pricePerKm) {
        ValidationHelper.validateValueInRange(pricePerKm,
                                              PRICE_MIN_VALUE,
                                              PRICE_MAX_VALUE,
                                              INVALID_PRICE);
    }
}
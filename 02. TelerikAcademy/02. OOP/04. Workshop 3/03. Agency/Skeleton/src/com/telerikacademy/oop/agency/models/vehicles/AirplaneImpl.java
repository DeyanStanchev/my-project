package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.vehicles.contracts.Airplane;
import com.telerikacademy.oop.agency.models.vehicles.enums.VehicleType;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public class AirplaneImpl extends VehicleBase implements Airplane {

    public static final String INVALID_PASSENGER_CAPACITY = "An airplane cannot have less than 1 passenger or more than 800 passengers.";
    public static final String INVALID_PRICE = "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!";
    public static final int PASSENGER_MIN_VALUE = 1;
    public static final int PASSENGER_MAX_VALUE = 800;
    public static final double PRICE_MIN_VALUE = 0.1;
    public static final double PRICE_MAX_VALUE = 2.5;

    private static final VehicleType vehicleType = VehicleType.AIR;

    private final boolean hasFreeFood;


    public AirplaneImpl(int id, int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(id, passengerCapacity, pricePerKilometer, vehicleType);
        this.hasFreeFood = hasFreeFood;
    }


    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }


    @Override
    public String getAsString() {
        return String.format("Airplane %s" +
                             "Has free food: %s%n",
                super.getAsString(),
                hasFreeFood());
    }


    @Override
    void validatePassengersCount(int passengerCapacity) {
        ValidationHelper.validateValueInRange(passengerCapacity,
                                              PASSENGER_MIN_VALUE,
                                              PASSENGER_MAX_VALUE,
                                              INVALID_PASSENGER_CAPACITY);
    }

    @Override
    void validatePricePerKm(double pricePerKm) {
        ValidationHelper.validateValueInRange(pricePerKm,
                                              PRICE_MIN_VALUE,
                                              PRICE_MAX_VALUE,
                                              INVALID_PRICE);
    }
}
package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    public static final String INVALID_NEGATIVE_PRICE = "Value of 'costs' must be a positive number. Actual value: %.2f.";

    private final Journey journey;
    private double administrativeCosts;
    private final int ticketId;


    public TicketImpl(int id, Journey journey, double costs) {
        this.ticketId = id;
        this.journey = journey;
        setAdministrativeCosts(costs);
    }

    private void setAdministrativeCosts(double administrativeCosts) {
        if (administrativeCosts < 0) {
            throw new InvalidUserInputException(String.format(INVALID_NEGATIVE_PRICE, administrativeCosts));
        }
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return administrativeCosts * journey.calculateTravelCosts();
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public int getId() {
        return ticketId;
    }

    @Override
    public String getAsString() {
        return String.format("Ticket ----%n" +
                             "Destination: %s%n" +
                             "Price: %.2f%n",
                getJourney().getDestination(),
                calculatePrice());
    }
}

package com.telerikacademy.oop.agency.commands.creation;

import com.telerikacademy.oop.agency.commands.contracts.Command;
import com.telerikacademy.oop.agency.core.contracts.AgencyRepository;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

import java.util.List;

import static com.telerikacademy.oop.agency.utils.ParsingHelpers.tryParseDouble;
import static com.telerikacademy.oop.agency.utils.ParsingHelpers.tryParseInteger;

public class CreateTicketCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String TICKET_CREATED_MESSAGE = "Ticket with ID %d was created.";

    private final AgencyRepository agencyRepository;

    private double administrativeCosts;
    private int journeyId;

    public CreateTicketCommand(AgencyRepository agencyRepository) {
        this.agencyRepository=agencyRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters,EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Journey journey = agencyRepository.findElementById(agencyRepository.getJourneys(),journeyId);
        Ticket ticket = agencyRepository.createTicket(journey,administrativeCosts);

        return String.format(TICKET_CREATED_MESSAGE, ticket.getId());
    }
    private void parseParameters(List<String> parameters) {
        journeyId = tryParseInteger(parameters.get(0),"journey id");
        administrativeCosts = tryParseDouble(parameters.get(1),"cost");

    }
}
package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;
import com.telerikacademy.oop.agency.models.vehicles.enums.VehicleType;

public abstract class VehicleBase implements Vehicle {

    private int passengerCapacity;
    private VehicleType vehicleType;
    private double pricePerKm;
    private final int vehicleId;

    public VehicleBase(int vehicleId,int  passengerCapacity, double pricePerKm,VehicleType vehicleType) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKm(pricePerKm);
        setVehicleType(vehicleType);
        this.vehicleId=vehicleId;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        validatePassengersCount(passengerCapacity);
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    private void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    private void setPricePerKm(double price) {
        validatePricePerKm(price);
        this.pricePerKm = price;
    }
    @Override
    public double getPricePerKilometer() {
        return pricePerKm;
    }

    @Override
    public String getAsString() {
      return String.format("----%n" +
                           "Passenger capacity: %d%n" +
                           "Price per kilometer: %.2f%n" +
                           "Vehicle type: %s%n",
              getPassengerCapacity(),
              getPricePerKilometer(),
              getType());
    }

    @Override
    public int getId() {
        return vehicleId;
    }


    abstract void validatePassengersCount(int passengerCapacity);

    abstract void validatePricePerKm(double pricePerKm);
}



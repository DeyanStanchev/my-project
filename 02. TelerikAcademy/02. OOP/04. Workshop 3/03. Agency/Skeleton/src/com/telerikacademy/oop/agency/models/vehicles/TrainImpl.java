package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.vehicles.contracts.Train;
import com.telerikacademy.oop.agency.models.vehicles.enums.VehicleType;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public class TrainImpl extends VehicleBase implements Train {

    public static final String INVALID_PASSENGERS_CAPACITY = "A train cannot have less than 30 passengers or more than 150 passengers.";
    public static final String INVALID_PRICE = "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!";
    public static final String INVALID_CARTS_COUNT = "A train cannot have less than 1 cart or more than 15 carts.";
    public static final int PASSENGER_MIN_VALUE = 30;
    public static final int PASSENGER_MAX_VALUE = 150;
    public static final int CARTS_MIN_VALUE = 1;
    public static final int CARTS_MAX_VALUE = 15;
    public static final double PRICE_MIN_VALUE = 0.1;
    public static final double PRICE_MAX_VALUE = 2.5;

    private static final VehicleType vehicleType = VehicleType.LAND;
    private int carts;

    public TrainImpl(int id, int passengerCapacity, double pricePerKilometer, int carts) {
        super(id,passengerCapacity,pricePerKilometer,vehicleType);
        setCarts(carts);
    }

    private void setCarts(int carts) {
        ValidationHelper.validateValueInRange(carts,CARTS_MIN_VALUE,CARTS_MAX_VALUE,INVALID_CARTS_COUNT);
        this.carts = carts;
    }

    @Override
    public int getCarts() {
        return carts;
    }


    @Override
    public String getAsString() {
        return String.format("Train %s" +
                             "Carts amount: %s%n",
                super.getAsString(),
                getCarts());
    }

    @Override
    void validatePassengersCount(int passengersCapacity) {
        ValidationHelper.validateValueInRange(passengersCapacity,
                                              PASSENGER_MIN_VALUE,
                                              PASSENGER_MAX_VALUE,
                                              INVALID_PASSENGERS_CAPACITY);
    }

    @Override
    void validatePricePerKm(double pricePerKm) {
        ValidationHelper.validateValueInRange(pricePerKm,
                                              PRICE_MIN_VALUE,
                                              PRICE_MAX_VALUE,
                                              INVALID_PRICE);
    }
}
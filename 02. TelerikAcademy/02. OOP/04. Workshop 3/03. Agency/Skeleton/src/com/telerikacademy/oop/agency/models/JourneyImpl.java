package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public class JourneyImpl implements Journey {
    public static final String INVALID_START_LOCATION_LENGTH = "The StartingLocation's length cannot be less than 5 or more than 25 symbols long.";
    public static final String INVALID_DESTINATION_LENGTH = "The Destination's length cannot be less than 5 or more than 25 symbols long.";
    public static final String INVALID_DISTANCE_RANGE = "The Distance cannot be less than 5 or more than 5000 kilometers.";
    public static final int START_LOCATION_MIN_LENGTH = 5;
    public static final int START_LOCATION_MAX_LENGTH = 25;
    public static final int DESTINATION_MIN_LENGTH = 5;
    public static final int DESTINATION_MAX_LENGTH = 25;
    public static final int DISTANCE_MIN_VALUE = 5;
    public static final int DISTANCE_MAX_VALUE = 5000;

    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;
    private final int journeyId;

    public JourneyImpl(int id, String startLocation, String destination, int distance, Vehicle vehicle) {
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        setVehicle(vehicle);
        this.journeyId = id;
    }

    private void setStartLocation(String startLocation) {
        ValidationHelper.validateStringLength(startLocation,
                                              START_LOCATION_MIN_LENGTH,
                                              START_LOCATION_MAX_LENGTH,
                                              INVALID_START_LOCATION_LENGTH);
        this.startLocation = startLocation;
    }

    private void setDestination(String destination) {
        ValidationHelper.validateStringLength(destination,
                                              DESTINATION_MIN_LENGTH,
                                              DESTINATION_MAX_LENGTH,
                                              INVALID_DESTINATION_LENGTH);
        this.destination = destination;
    }

    private void setDistance(int distance) {
        ValidationHelper.validateValueInRange(distance,
                                              DISTANCE_MIN_VALUE,
                                              DISTANCE_MAX_VALUE,
                                              INVALID_DISTANCE_RANGE);
        this.distance = distance;
    }

    private void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public String getStartLocation() {
        return startLocation;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }

    @Override
    public String getDestination() {
        return destination;
    }

    @Override
    public double calculateTravelCosts() {
        return distance * vehicle.getPricePerKilometer();
    }

    @Override
    public int getId() {
        return journeyId;
    }

    @Override
    public String getAsString() {
        return String.format("Journey ----%n" +
                        "Start location: %s%n" +
                        "Destination: %s%n" +
                        "Distance: %d%n" +
                        "Vehicle type: %s%n" +
                        "Travel costs: %.2f%n",
                getStartLocation(),
                getDestination(),
                getDistance(),
                getVehicle().getType(),
                calculateTravelCosts());
    }
}
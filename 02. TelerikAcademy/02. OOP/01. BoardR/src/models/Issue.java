package models;

import enums.Status;

import java.time.LocalDate;

public class Issue extends BoardItem {

    private static final String INITIAL_STATUS_ISSUE = "Open";
    private static final String MIN_REVERT_STATUS_MESSAGE = "Issue status already at Open";
    private static final String MAX_ADVANCE_STATUS_MESSAGE = "Issue status already at Verified";
    private static final String STATUS_CHANGE_MESSAGE = "Issue status set to";

    private final String description;

    public Issue(String title,String description, LocalDate dueDate) {
        super(title, dueDate, INITIAL_STATUS_ISSUE);
        this.description = description == null ? "No description" :  description;
        logEvent("Item Created - " + viewInfo());
    }


    public String getDescription() {
        return description;
    }

    @Override
    public String viewInfo(){
        return String.format("Issue: %s , Description: %s",super.viewInfo(),getDescription());
    }

    @Override
    public void advanceStatus() {
        if (getStatus().equals(Status.VERIFIED)) {
            logEvent(MAX_ADVANCE_STATUS_MESSAGE);
            return;
        }
        logEvent(String.format("%s %s", STATUS_CHANGE_MESSAGE,convertStatus(Status.VERIFIED)));
        setStatus("Verified");
    }

    @Override
    public void revertStatus() {
        if(getStatus().equals(Status.OPEN)){
            events.add(new EventLog(MIN_REVERT_STATUS_MESSAGE));
            return;
        }
        logEvent(String.format("%s %s", STATUS_CHANGE_MESSAGE,convertStatus(Status.OPEN)));
        setStatus("Open");
    }
}

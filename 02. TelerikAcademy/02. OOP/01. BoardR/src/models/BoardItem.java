package models;

import enums.Status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import utils.*;

public abstract class BoardItem {
    private static final int MIN_TITLE_LENGTH = 5;
    private static final int MAX_TITLE_LENGTH = 30;
    private static final String INVALID_TITLE_NAME_MESSAGE = "Title length must be between 5 and 30 symbols.";
    private static final String INVALID_DUE_DATE_MESSAGE = "Due date can`t be in the past.";
    private static final String INVALID_STATUS_MESSAGE = "Invalid status.";
    private static final String TITLE_CHANGE_MESSAGE = "Title changed from %s to %s";
    private static final String DUE_DATE_CHANGE_MESSAGE = "Due Date changed from %s to %s";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    private String title;
    private LocalDate dueDate;
    private Status status;
    protected final Status[] statuses = Status.values();
    protected final List<EventLog> events = new ArrayList<>();


    public BoardItem(String title, LocalDate dueDate, String status) {
        setTitle(title);
        setDueDate(dueDate);
        setStatus(status);
    }

    private List<EventLog> getEvents() {
        return new ArrayList<EventLog>(events);
    }

    public void setTitle(String title) {
        ValidationHelpers.validateStringLength(title,MIN_TITLE_LENGTH,MAX_TITLE_LENGTH,INVALID_TITLE_NAME_MESSAGE);
        if (!events.isEmpty()) {
            logEvent(String.format(TITLE_CHANGE_MESSAGE,getTitle(), title));
        }
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDueDate(LocalDate dueDate) {
        ValidationHelpers.validateDueDate(dueDate,INVALID_DUE_DATE_MESSAGE);
        if (!events.isEmpty()) {
           logEvent(String.format(DUE_DATE_CHANGE_MESSAGE, getDueDate(), dueDate));
        }
        this.dueDate = dueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    protected void setStatus(String status) {
        this.status = ParsingHelpers.tryParseStatus(status,INVALID_STATUS_MESSAGE);
    }

    public Status getStatus() {
        return status;
    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", getTitle(), convertStatus(status), dueDate.format(FORMATTER));
    }

    public String getHistory(){
        StringBuilder builder = new StringBuilder();

        for (EventLog event : events) {
            builder.append(event.viewInfo()).append(System.lineSeparator());
        }
        return builder.toString();
    }

    protected abstract void advanceStatus();

    protected abstract void revertStatus();


    protected String convertStatus(Status status) {
        switch (status) {
            case OPEN:
                return "Open";
            case TODO:
                return "ToDo";
            case INPROGRESS:
                return "InProgress";
            case DONE:
                return "Done";
            case VERIFIED:
                return "Verified";
        }
        throw new IllegalArgumentException(INVALID_STATUS_MESSAGE);
    }

    protected void logEvent(String event){
        events.add(new EventLog(event));
    }
}


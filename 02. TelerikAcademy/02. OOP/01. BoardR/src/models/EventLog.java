package models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;


public final class EventLog {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    private final String event;
    private final LocalDateTime timeOfEvent;

    public EventLog(String event){
        validateEventNameLength(event);
        this.event = event;
        timeOfEvent = LocalDateTime.now();
    }

    private void validateEventNameLength(String event) {
        if(event==null){
            throw new IllegalArgumentException("Event can`t be null");
        }
    }
    public String viewInfo(){
        return String.format("[%s] %s",timeOfEvent.format(formatter),event);
    }
}

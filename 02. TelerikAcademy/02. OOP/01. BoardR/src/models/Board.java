package models;

import contracts.Logger;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private final List<BoardItem> items;
    private int totalItems = 0;

    public Board() {
        items = new ArrayList<>();
    }

    public ArrayList<BoardItem> getItems() {
        return new ArrayList<>(items);
    }

    public void addItem(BoardItem item){
        if(items.contains(item)){
            throw new IllegalArgumentException("Item is already in the list.");
        }
        items.add(item);
        totalItems++;
    }
    public int totalItems(){
        return totalItems;
    }

    public void displayHistory(Logger logger){
        for (BoardItem item : items) {
            logger.log(item.getHistory());
        }
    }
}

package models;

import enums.Status;
import utils.ValidationHelpers;

import java.time.LocalDate;

public class Task extends BoardItem {
    private static final int MIN_ASSIGNEE_NAME_LENGTH = 5;
    private static final int MAX_ASSIGNEE_NAME_LENGTH = 30;
    private static final String INVALID_ASSIGNEE_NAME_MESSAGE = "Please provide a name between 5 and 30 symbols.";
    private static final String MIN_REVERT_MESSAGE = "Can`t revert, already at To do";
    private static final String MAX_ADVANCE_MESSAGE = "Can`t advance, already at Verified";
    private static final String STATUS_CHANGE_MESSAGE = "Task status changed from %s to %s";
    private static final String ASSIGNEE_CHANGE_MESSAGE = "Assignee changed from %s to %s";
    private static final String INITIAL_STATUS_TASK = "Todo";

    private String assignee;

    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate, INITIAL_STATUS_TASK);
        setAssignee(assignee);
        logEvent("Item Created - " + viewInfo());
    }

    public void setAssignee(String assignee) {
        ValidationHelpers.validateStringLength(assignee, MIN_ASSIGNEE_NAME_LENGTH, MAX_ASSIGNEE_NAME_LENGTH, INVALID_ASSIGNEE_NAME_MESSAGE);
        if (!events.isEmpty()) {
            events.add(new EventLog(String.format(ASSIGNEE_CHANGE_MESSAGE, getAssignee(), assignee)));
        }
        this.assignee = assignee;
    }

    public String getAssignee() {
        return assignee;
    }

    @Override
    public String viewInfo() {
        return String.format("Task: %s, Assignee: %s", super.viewInfo(), getAssignee());
    }

    @Override
    public void advanceStatus() {
        Status tmp = getStatus();
        if (getStatus().equals(Status.VERIFIED)) {
            events.add(new EventLog(MAX_ADVANCE_MESSAGE));
            return;
        }
        events.add(new EventLog(String.format(STATUS_CHANGE_MESSAGE, convertStatus(getStatus()), convertStatus(statuses[(tmp.ordinal() + 1)]))));
        setStatus(convertStatus(statuses[(tmp.ordinal() + 1)]));
    }


    @Override
    public void revertStatus() {
        Status tmp = getStatus();
        if (getStatus().equals(Status.TODO)) {
            logEvent(MIN_REVERT_MESSAGE);
            return;
        }
        logEvent(String.format(STATUS_CHANGE_MESSAGE, convertStatus(getStatus()), convertStatus(statuses[(tmp.ordinal() - 1)])));
        setStatus(convertStatus(statuses[(tmp.ordinal() - 1)]));

    }
}


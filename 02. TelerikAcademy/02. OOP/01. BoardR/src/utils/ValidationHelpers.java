package utils;

import java.time.LocalDate;

public class ValidationHelpers {
    public static void validateStringLength(String input, int minLength , int maxLength,String errorMessage) {
        if (input == null || input.length() < minLength || input.length() > maxLength) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public static void validateDueDate(LocalDate dueDate,String errorMessage) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException(errorMessage);
        }
    }
}

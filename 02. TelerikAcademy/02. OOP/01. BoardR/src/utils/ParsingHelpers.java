package utils;

import enums.Status;

public class ParsingHelpers {

    public static Status tryParseStatus(String valueToParse, String errorMessage) {
        try {
            return Status.valueOf(valueToParse.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format(errorMessage, valueToParse));
        }
    }
}

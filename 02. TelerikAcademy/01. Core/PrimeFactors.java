package telerikAcademy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class PrimeFactors {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = Integer.parseInt(scanner.nextLine());
        scanner.close();
        ArrayList<Integer> primeFactors = new ArrayList<>();
        while(number!=1){
            for (int i = 2; i <= number ; i++) {
                boolean isPrime = true;
                for (int j = 2; j <= i / 2; j++) {
                    if (i % j == 0) {
                        isPrime = false;
                        break;
                    }
                }
                if(isPrime) {
                    if (number % i == 0) {
                        primeFactors.add(i);
                        number /= i;
                        break;
                    }
                }
            }
        }
        Collections.sort(primeFactors);
        for (int factor:primeFactors) {
            System.out.println(factor);
        }
    }
}

package telerikAcademy;

import java.util.Scanner;

public class ReverseDigits {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        char[] digits = number.toCharArray();
        for (int i = digits.length-1; i >= 0; i--) {
            if(digits[i]!='-'){
                System.out.print(digits[i]);
            }
        }
    }
}

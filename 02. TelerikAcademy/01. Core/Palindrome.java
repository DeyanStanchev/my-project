package hackerRank;

import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String word = scanner.next();
        boolean isPalindrome = false;

        String reversedWord = reverseString(word);
        if(word.equals(reversedWord)){
            isPalindrome=true;
        }
        if(isPalindrome){
            System.out.println("Yes");
        }else{
            System.out.println("No");
        }
    }

    private static String reverseString(String word) {
        StringBuilder sb = new StringBuilder();
        for (int i = word.length()-1; i >=0; i--) {
            sb.append(word.charAt(i));
        }
        return sb.toString();
    }
}

package telerikAcademy;

import java.util.Scanner;

public class MostFrequentNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            numbers[i]= Integer.parseInt(scanner.nextLine());
        }
        int mostFrequent = 0;
        int timesRepeated = 0;
        for (int i = 0; i < numbers.length; i++) {
            int tmp = numbers[i];
            int currentCount = 0;
            for (int j = 0; j <numbers.length ; j++) {
                if(tmp == numbers[j]){
                    currentCount++;
                }
                if(currentCount>timesRepeated){
                    mostFrequent = numbers[i];
                    timesRepeated=currentCount;
                }
            }
        }
        System.out.printf("%d (%d times)",mostFrequent,timesRepeated);
    }
}

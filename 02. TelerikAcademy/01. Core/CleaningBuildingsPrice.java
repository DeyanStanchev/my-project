package telerikAcademy;

import java.util.Scanner;

public class CleaningBuildingsPrice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberBuildings = Integer.parseInt(scanner.nextLine());

        double pricePerWindow = 0;
        double priceForBuilding = 0;
        double totalPrice = 0;

        for (int i = 0; i < numberBuildings; i++) {
            String input = scanner.nextLine();
            String[] inputSplit = input.split("[ ]");
            String buildingType = inputSplit[0];
            int numberOfFloors = Integer.parseInt(inputSplit[1]);
            int windowsPerFloor = Integer.parseInt(inputSplit[2]);
            switch (buildingType) {
                case "Residential":
                    pricePerWindow = 0.75;
                    break;
                case "Business":
                    pricePerWindow = 1.25;
                    break;
                case "Mall":
                    pricePerWindow = 2.0;
                    break;
                default:
                    System.out.println("Invalid building");
                    break;
            }
            priceForBuilding = numberOfFloors*windowsPerFloor*pricePerWindow;
            totalPrice+=priceForBuilding;
            System.out.printf("%.2f$%n",priceForBuilding);
        }
        System.out.printf("Total: %.2f$",totalPrice);
    }
}

package telerikAcademy;

import java.util.ArrayList;
import java.util.Scanner;

public class BelowAndAboveAVG {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] numbers = input.split("[,]");
        double avg = calculateSum(numbers);
        ArrayList<String> belowAvg = new ArrayList<>();
        ArrayList<String> aboveAvg = new ArrayList<>();
        for (int i = 0; i < numbers.length; i++) {
            int tmp = Integer.parseInt(numbers[i]);
            if (tmp < avg) {
                belowAvg.add(numbers[i]);
            } else if( tmp > avg) {
                aboveAvg.add(numbers[i]);
            }
        }
        System.out.printf("avg: %.2f%n", avg);
        System.out.print("below: ");
        printArray(belowAvg);
        System.out.print("above: ");
        printArray(aboveAvg);
    }

    private static void printArray(ArrayList<String> arr) {
        for (int i = 0; i < arr.size(); i++) {
            if (i != arr.size() - 1) {
                System.out.print(arr.get(i) + ",");
            } else {
                System.out.print(arr.get(i));
            }
        }
        System.out.println();
    }

    private static double calculateSum(String[] numbers) {
        double sum = 0;
        for (String number : numbers) {
            sum += Integer.parseInt(number);
        }
        return sum / numbers.length;
    }
}

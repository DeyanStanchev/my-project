package telerikAcademy;

import java.util.Scanner;

public class ReturnChangeInCoins {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double price = Double.parseDouble(scanner.nextLine());
        int convertedPrice = (int) (price * 100);
        int paid = Integer.parseInt(scanner.nextLine());
        scanner.close();
        int change = (paid*100) - convertedPrice;
        int counterLev = 0;
        int counterFifty = 0;
        int counterTwenty = 0;
        int counterTen = 0;
        int counterFive = 0;
        int counterTwo = 0;
        int counterOne = 0;
        while (change > 0) {
            if (change - 100 >= 0) {
                change -= 100;
                counterLev++;
            } else if (change - 50 >= 0) {
                change -= 50;
                counterFifty++;
            } else if (change - 20 >= 0) {
                change -=20;
                counterTwenty++;
            } else if (change - 10 >= 0) {
                change -= 10;
                counterTen++;
            } else if (change - 5 >= 0) {
                change -= 5;
                counterFive++;
            } else if (change - 2 >= 0) {
                change -=2;
                counterTwo++;
            } else if (change - 1 >= 0) {
                change -= 1;
                counterOne++;
            }
        }
        if (counterLev != 0) {
            System.out.printf("%d x 1 lev%n", counterLev);
        }
        if (counterFifty != 0) {
            System.out.printf("%d x 50 stotinki%n", counterFifty);
        }
        if (counterTwenty != 0) {
            System.out.printf("%d x 20 stotinki%n", counterTwenty);
        }
        if (counterTen != 0) {
            System.out.printf("%d x 10 stotinki%n", counterTen);
        }
        if (counterFive != 0) {
            System.out.printf("%d x 5 stotinki%n", counterFive);
        }
        if (counterTwo != 0) {
            System.out.printf("%d x 2 stotinki%n", counterTwo);
        }
        if (counterOne != 0) {
            System.out.printf("%d x 1 stotinka", counterOne);
        }
    }
}
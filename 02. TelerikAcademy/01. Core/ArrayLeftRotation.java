package hackerRank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ArrayLeftRotation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        int timesRotate = scanner.nextInt();
        ArrayList<Integer> array = new ArrayList<>();
        array = fillList(scanner,arrayLength);
        Collections.rotate(array,-timesRotate);
        printArray(array);
    }


    private static void printArray(ArrayList<Integer> arrayRotated) {
        for (int j : arrayRotated) {
            System.out.print(j+" ");
        }
    }

    private static ArrayList<Integer> fillList(Scanner scanner,int length) {
        ArrayList<Integer> array = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            array.add(scanner.nextInt());
        }
        return array;
    }
}

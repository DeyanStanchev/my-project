package telerikAcademy;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n  = Integer.parseInt(scanner.nextLine());
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++) {
            int counter = i+1;
            for (int j = 0; j < n; j++) {
                matrix[i][j]= counter++;
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
    }
}

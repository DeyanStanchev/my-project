package hackerRank;

import java.util.Scanner;

public class BalancedNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sum =0;
        while (scanner.hasNextLine()) {
            String number = scanner.nextLine();
            int tmp = 0;
            for (int i = 0; i <number.length() ; i++) {
                if(i!=1) {
                    int digit = Character.digit(number.charAt(i), 10);
                    tmp += digit;
                }
            }
            if(tmp!=Character.digit(number.charAt(1), 10)){
                break;
            }
            sum += Integer.parseInt(number);
        }
        System.out.println(sum);
    }
}



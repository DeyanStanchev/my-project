package telerikAcademy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class BiggestOfThreeNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double first = Double.parseDouble(scanner.nextLine());
        double second = Double.parseDouble(scanner.nextLine());
        double third = Double.parseDouble(scanner.nextLine());
        ArrayList<Double> sortedNumbers = new ArrayList<>();
        sortedNumbers.add(first);
        sortedNumbers.add(second);
        sortedNumbers.add(third);
        Collections.sort(sortedNumbers);
        double max = sortedNumbers.get(sortedNumbers.size()-1);
        System.out.printf("%.0f", max);
    }
}
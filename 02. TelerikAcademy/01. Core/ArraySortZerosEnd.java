package telerikAcademy;

import java.util.ArrayList;
import java.util.Scanner;

public class ArraySortZerosEnd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] numbers = input.split("[,]");
        ArrayList<String> sortedNumbers = new ArrayList<>();

        for (int i = 0; i < numbers.length; i++) {
            String tmp = numbers[i];
            if(!tmp.equals("0")){
                sortedNumbers.add(tmp);
            }
        }
        for (int i = 0; i < numbers.length; i++) {
            String tmp = numbers[i];
            if(tmp.equals("0")){
                sortedNumbers.add(tmp);
            }
        }
        printArray(sortedNumbers);
    }

    private static void printArray(ArrayList<String> arr) {
        for (int i = 0; i < arr.size(); i++) {
            if (i != arr.size() - 1) {
                System.out.print(arr.get(i) + ",");
            } else {
                System.out.print(arr.get(i));
            }
        }
        System.out.println();
    }
}




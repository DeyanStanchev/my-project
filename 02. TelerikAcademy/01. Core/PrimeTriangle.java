package hackerRank;

import java.util.List;
import java.util.Scanner;

public class PrimeTriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = Integer.parseInt(scanner.nextLine());

        for (int i = 1; i <= number; i++) {
            boolean isPrime = true;
            if (i == 1) {
                System.out.println(1);
                continue;
            }
            for (int j = 2; j <= i / 2; j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                for (int k = 1; k <= i; k++) {
                    boolean isItPrime = true;
                    if (k == 1) {
                        System.out.print(1);
                        continue;
                    }
                    for (int m = 2; m <= k / 2; m++) {
                        if (k % m == 0) {
                            isItPrime = false;
                        }
                    }
                    if (isItPrime) {
                        System.out.print(1);
                        }else{
                        System.out.print(0);
                    }
                }
                System.out.println();
            }
        }
    }
}



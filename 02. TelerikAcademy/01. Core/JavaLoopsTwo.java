package hackerRank;

import java.util.Scanner;

public class JavaLoopsTwo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int q = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < q; i++) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            int n = scanner.nextInt();
            int c = a;
            for (int j = 0; j < n; j++) {
                c+= Math.pow(2,j)*b;
                System.out.printf("%s ",c);
            }
            System.out.println();
        }
    }
}

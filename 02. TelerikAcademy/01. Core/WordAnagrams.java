package hackerRank;

import java.util.Arrays;
import java.util.Scanner;

public class WordAnagrams {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String word = scanner.nextLine();
        int numberWords = Integer.parseInt(scanner.nextLine());
        String[] wordsToCheck = new String[numberWords];
        for (int i = 0; i < wordsToCheck.length; i++) {
            wordsToCheck[i] = scanner.nextLine();
        }
        for (int i = 0; i < wordsToCheck.length; i++) {
            boolean isAnagram = checkForAnagram(word, wordsToCheck[i]);
            if(isAnagram){
                System.out.println("Yes");
            }else{
                System.out.println("No");
            }
        }

    }

    private static boolean checkForAnagram(String word, String checkWord) {
        char[] firstWord = word.toCharArray();
        char[] secondWord = checkWord.toCharArray();
        Arrays.sort(firstWord);
        Arrays.sort(secondWord);
        if (word.length() != checkWord.length()) {
            return false;
        }
        for (int i = 0; i < word.length(); i++) {
            if (firstWord[i] != secondWord[i]) {
                return false;
                }
            }
        return true;
        }
    }


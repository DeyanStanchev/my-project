package hackerRank;

import java.util.Scanner;

public class EndOfFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        int num = 1;
        while (scanner.hasNextLine()) {
            System.out.printf("%d %s%n", num, s);
            num++;
            s = scanner.nextLine();
        }
    }
}



package telerikAcademy;

import javax.print.attribute.IntegerSyntax;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class LargestThreeValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int current = Integer.parseInt(scanner.nextLine());
            numbers.add(current);
        }
        Collections.sort(numbers);
        System.out.printf("%d, %d and %d",numbers.get(numbers.size()-1),numbers.get(numbers.size()-2),numbers.get(numbers.size()-3));
    }
}

package hackerRank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JumpingOnClouds {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int cloudsCount = Integer.parseInt(scanner.nextLine());
        List<Integer> clouds = new ArrayList<>();
        for (int i = 0; i < cloudsCount; i++) {
            clouds.add(scanner.nextInt());
        }
        int minJumps = getMinJumps(clouds);
        System.out.println(minJumps);
    }

    private static int getMinJumps(List<Integer> clouds) {
        int minJumps = 0;
        for (int i = 0; i < clouds.size(); i++) {
            if (i < clouds.size() - 2) {
                if (clouds.get(i + 2) == 0) {
                    minJumps++;
                    i++;
                }else if (clouds.get(i + 1) == 0) {
                    minJumps++;
                }
            } else if (i < clouds.size() - 1) {
                if (clouds.get(i + 1) == 0) {
                    minJumps++;
                }
            }
        }
        return minJumps;
    }
}

package telerikAcademy;

import java.util.ArrayList;
import java.util.Scanner;

public class IsListSorted {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < n; i++) {
            String input = scanner.nextLine();
            String[] digits = input.split("[,]");
            ArrayList<Integer> tmp = fillList(digits);
            boolean isItSorted = isSorted(tmp);
            System.out.println(isItSorted);
        }
    }

    private static boolean isSorted(ArrayList<Integer> tmp) {
        boolean isSorted = true;
        for (int i = 0; i <= tmp.size()-2; i++) {
            if(tmp.get(i)>tmp.get(i+1)){
                isSorted=false;
                return isSorted;
            }
        }
        return isSorted;
    }

    private static ArrayList<Integer> fillList(String[] digits) {
        ArrayList<Integer> listDigits = new ArrayList<>();
        for (int i = 0; i < digits.length; i++) {
            listDigits.add(Integer.parseInt(digits[i]));
        }
        return listDigits;
    }
}

package hackerRank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HourglassSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<List<Integer>> matrix = fillList(scanner);

        int sum = Integer.MIN_VALUE;

        for (int i = 0; i < matrix.size()-2; i++) {
            for (int j = 0; j < matrix.size()-2; j++) {
                int tmpSum = matrix.get(i).get(j) + matrix.get(i).get(j + 1) + matrix.get(i).get(j + 2) + matrix.get(i + 1).get(j + 1) + matrix.get(i + 2).get(j)
                        + matrix.get(i + 2).get(j + 1) + matrix.get(i + 2).get(j + 2);
                if(tmpSum>sum){
                    sum=tmpSum;
                }
            }
        }
        System.out.println(sum);
    }

    private static List<List<Integer>> fillList(Scanner scanner) {
        List<List<Integer>> matrix = new ArrayList<List<Integer>>();
        for (int i = 0; i < 6; i++) {
            List<Integer> row = new ArrayList<>();
            for (int j = 0; j < 6; j++) {
                row.add(scanner.nextInt());
            }
            matrix.add(row);
        }
        return matrix;
    }
}


package telerikAcademy;

import java.util.Scanner;

public class BiggestOfFive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double[] array = new double[3];
        double maxNum = -201;
        for (int i = 0; i < 3; i++) {
            array[i] = Double.parseDouble(scanner.nextLine());
        }
        for (int i = 0; i < 3; i++) {
            if (array[i] <= 200 && array[i] >= -200) {
                for (i = 0; i < 3; i++) {

                    if (array[i] > maxNum) {
                        maxNum = array[i];
                    }
                }
                if (maxNum % 1 == 0) {
                    System.out.printf("%.0f", maxNum);
                }
                else{
                    System.out.println(maxNum);
                }
            }
        }
    }
}

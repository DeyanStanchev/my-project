package hackerRank;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int number = Integer.parseInt(scanner.nextLine());
        int[] digits = new int[3];
        for (int i = 0; i < digits.length; i++) {
            digits[i] = number % 10;
            number /= 10;
        }
        int sum = Integer.MIN_VALUE;

        if (digits[0] + digits[1] + digits[2] > sum) {
            sum = digits[0] + digits[1] + digits[2];
        }
        if (digits[0] * digits[1] + digits[2] > sum) {
            sum = digits[0] * digits[1] + digits[2];
        }
        if (digits[0] + digits[1] * digits[2] > sum) {
            sum = digits[0] + digits[1] * digits[2];
        }
        if (digits[0] * digits[1] * digits[2] > sum) {
            sum = digits[0] * digits[1] * digits[2];
        }

        System.out.println(sum);
    }
}

package telerikAcademy;

import java.util.ArrayList;
import java.util.Scanner;

public class ConvertDegrees {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] numbers = input.split("[ ]");
        ArrayList<Double> numbersList = convertToDouble(numbers);

        for (Double number:numbersList) {
            double newTemp = number*1.8+32;
            System.out.printf("%.0f%n",newTemp);
        }
    }
    private static ArrayList<Double> convertToDouble(String[] numbers) {
        ArrayList<Double> numbersConverted = new ArrayList<>();
        for (String number:numbers) {
            numbersConverted.add(Double.parseDouble(number));
        }
        return numbersConverted;
    }
}


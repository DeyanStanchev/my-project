package telerikAcademy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SortNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] digits= input.split("[, ]+");
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < digits.length; i++) {
            numbers.add(Integer.parseInt(digits[i]));
        }
        Collections.sort(numbers);
        for (int i = numbers.size()-1; i >=0 ; i--) {
            if (i != 0) {
                System.out.print(numbers.get(i) + ", ");
            } else {
                System.out.print(numbers.get(i));
            }
        }
    }
}

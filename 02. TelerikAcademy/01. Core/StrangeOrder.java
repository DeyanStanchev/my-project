package telerikAcademy;

import java.util.ArrayList;
import java.util.Scanner;

public class StrangeOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        String[] numbers = input.split("[,]");
        ArrayList<Integer> numbersList = new ArrayList<>();
        for (int i = 0; i < numbers.length; i++) {
            int tmp = Integer.parseInt(numbers[i]);
            if(tmp<0){
                numbersList.add(tmp);
            }
        }
        for (int i = 0; i < numbers.length; i++) {
            int tmp = Integer.parseInt(numbers[i]);
            if(tmp==0){
                numbersList.add(tmp);
            }
        }
        for (int i = 0; i < numbers.length; i++) {
            int tmp = Integer.parseInt(numbers[i]);
            if(tmp>0){
                numbersList.add(tmp);
            }
        }
        for (int i = 0; i <numbersList.size() ; i++) {
            if(i!=numbersList.size()-1){
                System.out.print(numbersList.get(i)+",");
            }else{
                System.out.print(numbersList.get(i));
            }
        }
    }
}

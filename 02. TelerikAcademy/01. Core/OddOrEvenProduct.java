package telerikAcademy;

import java.util.Scanner;

public class OddOrEvenProduct {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int oddProduct = 1;
        int evenProduct = 1;
        for (int i = 1; i <= n; i++) {
            int number = Integer.parseInt(scanner.nextLine());
            if(i %2 == 0){
                evenProduct*=number;
            }else{
                oddProduct*=number;
            }
        }
        if ( evenProduct == oddProduct){
            System.out.println("yes "+evenProduct);
        }else{
            System.out.printf("no %d %d",oddProduct,evenProduct);
        }
    }
}

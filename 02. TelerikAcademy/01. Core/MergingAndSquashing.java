package hackerRank;

import java.util.Scanner;

public class MergingAndSquashing {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[] numbers = new int[n];
        for (int i = 0; i <numbers.length; i++) {
            numbers[i]=Integer.parseInt(scanner.nextLine());
        }
        int[] mergedNums = new int[n-1];
        int[] squashedNums = new int[n-1];
        for (int i = 0; i < numbers.length-1; i++) {
            mergedNums[i]= merge(numbers[i],numbers[i+1]);
            squashedNums[i]= squash(numbers[i],numbers[i+1]);
        }
        printArray(mergedNums);
        printArray(squashedNums);
    }
    public static void printArray(int[] array){
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]+" ");
        }
        System.out.println();
    }
    public static int squash (int left , int right){
        int i = 0;
        int[] tmp = new int[4];
        while(left>0){
            tmp[i]=left%10;
            left/=10;
            i++;
        }
        while(right>0){
            tmp[i]=right%10;
            right/=10;
            i++;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(tmp[1]);
        if(tmp[0]+tmp[3]>=10){
            int digit = (tmp[0]+tmp[3]) % 10;
            sb.append(digit);
        }else{
            sb.append(tmp[0]+tmp[3]);
        }
        sb.append(tmp[2]);
        return Integer.parseInt(sb.toString());
    }
    public static int merge(int left, int right){
        int i = 0;
        int[] tmp = new int[2];
        tmp[0] = left%10;
        int digit = right%10;
        right/=10;
        tmp[1] = right;

        String sb = String.valueOf(tmp[0]) + tmp[1];
        return Integer.parseInt(sb);
    }
}

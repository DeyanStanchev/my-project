package telerikAcademy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ArraySearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] numbers = input.split("[,]");
        ArrayList<Integer> numbersList = convertToInteger(numbers);
        ArrayList<Integer> excludedList = new ArrayList<>();

        for (int i = 1; i <= numbers.length; i++) {
            if(!numbersList.contains(i)){
                excludedList.add(i);
            }
        }
        printArray(excludedList);
    }

    private static ArrayList<Integer> convertToInteger(String[] numbers) {
        ArrayList<Integer> numbersConverted = new ArrayList<>();
        for (String number:numbers) {
            numbersConverted.add(Integer.parseInt(number));
        }
        return numbersConverted;
    }

    private static void printArray(ArrayList<Integer> arr) {
        for (int i = 0; i < arr.size(); i++) {
            if (i != arr.size() - 1) {
                System.out.print(arr.get(i) + ",");
            } else {
                System.out.print(arr.get(i));
            }
        }
        System.out.println();
    }
}




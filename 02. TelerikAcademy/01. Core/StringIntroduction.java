package hackerRank;

import java.util.Scanner;

public class StringIntroduction {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String A = scanner.next();
        String B = scanner.next();

        int length = A.length() + B.length();
        String answer = "";

        if( A.compareToIgnoreCase(B) > 0){
            answer = "Yes";
        }else{
            answer= "No";
        }
        String firstWord = A.substring(0,1).toUpperCase();
        String secondWord = B.substring(0,1).toUpperCase();

        System.out.println(length);
        System.out.println(answer);
        System.out.println(firstWord+A.substring(1)+" "+secondWord+B.substring(1));
    }
}

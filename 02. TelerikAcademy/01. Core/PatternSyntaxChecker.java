package hackerRank;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class PatternSyntaxChecker {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < number; i++) {
            String some = scanner.nextLine();
            try{
                String test = Pattern.compile(some).toString();
                System.out.println("Valid");
            }catch(PatternSyntaxException ptn){
                System.out.println("Invalid");
            }
        }
    }
}

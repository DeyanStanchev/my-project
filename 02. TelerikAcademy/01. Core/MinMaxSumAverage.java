package telerikAcademy;

import java.util.Scanner;

public class MinMaxSumAverage {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        double min = Double.MAX_VALUE;
        double max = -Double.MAX_VALUE;
        double sum = 0;
        double avg = 0;
        for (int i = 0; i < n; i++) {
            double current = Double.parseDouble(scanner.nextLine());
            if (current < min) {
                min = current;
            }
            if (current > max) {
                max = current;
            }
            sum += current;
        }
        avg = sum / n;
        System.out.printf("%.2f%n",min);
        System.out.printf("%.2f%n",max);
        System.out.printf("%.2f%n",sum);
        System.out.printf("%.2f%n",avg);
    }
}


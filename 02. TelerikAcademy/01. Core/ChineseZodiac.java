package telerikAcademy;

import java.util.Scanner;

public class ChineseZodiac {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int year = Integer.parseInt(scanner.nextLine());
        String chineseYear = "";
        if(Math.abs(year-2000)%12==0){
            chineseYear = "Dragon";
        }else if(Math.abs(year-2001)%12==0){
            chineseYear = "Snake";
        }else if(Math.abs(year-2002)%12==0){
            chineseYear = "Horse";
        }else if(Math.abs(year-2003)%12==0){
            chineseYear = "Sheep";
        }else if(Math.abs(year-2004)%12==0){
            chineseYear = "Monkey";
        }else if(Math.abs(year-2005)%12==0){
            chineseYear = "Rooster";
        }else if(Math.abs(year-2006)%12==0){
            chineseYear = "Dog";
        }else if(Math.abs(year-2007)%12==0){
            chineseYear = "Pig";
        }else if(Math.abs(year-2008)%12==0){
            chineseYear = "Rat";
        }else if(Math.abs(year-2009)%12==0){
            chineseYear = "Ox";
        }else if(Math.abs(year-2010)%12==0){
            chineseYear = "Tiger";
        }else if(Math.abs(year-2011)%12==0){
            chineseYear = "Hare";
        }
        System.out.println(chineseYear);
    }
}

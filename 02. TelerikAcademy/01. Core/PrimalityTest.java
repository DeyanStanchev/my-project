package hackerRank;

import java.math.BigInteger;
import java.util.Scanner;

public class PrimalityTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String n = scanner.nextLine();
        BigInteger number = new BigInteger(n);
        boolean isPrime = number.isProbablePrime(1);
        if(isPrime){
            System.out.println("prime");
        }else {
            System.out.println("not prime");
        }
    }
}

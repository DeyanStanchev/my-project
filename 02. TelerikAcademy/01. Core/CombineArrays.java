package telerikAcademy;

import java.util.ArrayList;
import java.util.Scanner;

public class CombineArrays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String firstInput = scanner.nextLine();
        String secondInput = scanner.nextLine();
        String[] firstInputDigits = firstInput.split("[, ]+");
        String[] secondInputDigits = secondInput.split("[, ]+");
        ArrayList<String> digits = new ArrayList<>();
        for (int i = 0; i < firstInputDigits.length; i++) {
            digits.add(firstInputDigits[i]);
            digits.add(secondInputDigits[i]);
        }
        for (int i = 0; i < digits.size(); i++) {
            if(i!= digits.size()-1){
                System.out.print(digits.get(i)+",");
            }else{
                System.out.print(digits.get(i));
            }
        }
    }
}

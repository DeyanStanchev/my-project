package hackerRank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SmallestAndLargest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        int k = scan.nextInt();
        scan.close();

        System.out.println(getSmallestAndLargest(s, k));
    }

    private static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";
        ArrayList<String> subs = new ArrayList<>();
        for (int i = 0; i < s.length()-k+1; i++) {
            String sub = s.substring(i,i+k);
            subs.add(sub);
        }
        Collections.sort(subs);
        smallest = subs.get(0);
        largest = subs.get(subs.size()-1);

        return smallest + "\n" + largest;
    }
}


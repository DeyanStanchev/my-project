package telerikAcademy;

import java.util.Scanner;

public class DiffChecker {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String first = scanner.nextLine();
        String second = scanner.nextLine();
        String[] firstArray = first.split("[ ]");
        String[] secondArray = second.split("[ ]");
        int biggerLength = Math.max(firstArray.length, secondArray.length);

        for (int i = 0; i < biggerLength; i++) {
            String tmp = "";
            String tmpTwo = "";
            if (i < firstArray.length) {
                tmp = firstArray[i];
            } else {
                tmp = "x";
            }
            if (i < secondArray.length) {
                tmpTwo = secondArray[i];
            } else {
                tmpTwo = "x";
            }
            if(tmp.equals(tmpTwo)){
                System.out.printf("+ %s %s%n",tmp,tmpTwo);
            }else{
                System.out.printf("- %s %s%n",tmp,tmpTwo);
            }
        }
    }
}

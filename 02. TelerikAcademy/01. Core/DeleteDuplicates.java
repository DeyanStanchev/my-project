package telerikAcademy;

import java.util.*;
import java.util.stream.Collectors;

public class DeleteDuplicates {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] inputArray = input.split("[,]");
        ArrayList<String> listInput = new ArrayList<>();
        listInput.addAll(Arrays.asList(inputArray));
        List<String> deListInput = listInput.stream().distinct().collect(Collectors.toList());

        for (int i = 0; i <deListInput.size() ; i++) {
            if(i!=deListInput.size()-1){
                System.out.print(deListInput.get(i)+",");
            }else{
                System.out.print(deListInput.get(i));
            }
        }
    }
}

package telerikAcademy;

import java.util.Scanner;

public class SortedNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int first = Integer.parseInt(scanner.nextLine());
        int second = Integer.parseInt(scanner.nextLine());
        int third = Integer.parseInt(scanner.nextLine());
        int max = Math.max(Math.max(first, second), third);
        int secondMax = 0;
        int thirdMax = 0;


        if (max == first) {
            secondMax = Math.max(second, third);
        } else if (max == second) {
            secondMax = Math.max(first, third);
        } else {
            secondMax = Math.max(first, second);
        }
        if (max == first ) {
            thirdMax = Math.min(second,third);
        } else if (max == second) {
            thirdMax = Math.min(first,third);
        } else  {
            thirdMax = Math.min(first,second);
        }
        System.out.printf("%d %d %d", max, secondMax, thirdMax);
    }
}




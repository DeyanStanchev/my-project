package telerikAcademy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class RotateList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        int timesToRotate = Integer.parseInt(scanner.nextLine());;
        String[] inputDigits = input.split("[,]");
        ArrayList<Integer> listDigits = new ArrayList<>();

        for (int i = 0; i < inputDigits.length; i++) {
            listDigits.add(Integer.parseInt(inputDigits[i]));
        }
        Collections.rotate(listDigits,-timesToRotate);
        for (int i = 0; i <listDigits.size() ; i++) {
            if(i!=listDigits.size()-1){
                System.out.print(listDigits.get(i)+",");
            }else{
                System.out.print(listDigits.get(i));
            }
        }
    }
}

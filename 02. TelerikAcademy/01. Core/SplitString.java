package hackerRank;

import java.util.Scanner;

public class SplitString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String toSplit = scanner.nextLine();
        scanner.close();

        String[] parts = toSplit.split("[ !,?._'@]+");
        int length = parts.length;
        for (int i = 0; i < parts.length; i++) {
            if(parts[i].isEmpty()){
                length--;
            }
        }
        System.out.println(length);
        for (String part : parts) {
            if (!part.isEmpty()) {
                System.out.println(part);
            }
        }
    }
}

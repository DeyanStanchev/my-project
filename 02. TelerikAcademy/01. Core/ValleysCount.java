package hackerRank;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class ValleysCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int steps = Integer.parseInt(scanner.nextLine());
        String path = scanner.nextLine();

        int valleysCount = getValleysCount(steps, path);
        System.out.println(valleysCount);
    }

    public static int getValleysCount(int steps, String path) {
        int seaLevel = 0;
        int valleysCount = 0;
        for (int i = 0; i < steps; i++) {
            char step = path.charAt(i);
            switch (step) {
                case 'U':
                    seaLevel++;
                    break;
                case 'D':
                    seaLevel--;
                    if(seaLevel == -1){
                        valleysCount++;
                    }
                    break;
            }
        }
        return valleysCount;
    }
}

package telerikAcademy;

import java.util.Scanner;

public class ComplexSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        double x = Double.parseDouble(scanner.nextLine());
        double sum = 1;
        for (int i = 1; i <=n; i++) {
            int fac = 1;
            for (int j = 1; j <=i ; j++) {
                fac *=j;
            }
            double tmp = fac / (Math.pow(x,i));
            sum +=tmp;
        }
        System.out.printf("%.5f",sum);
    }
}

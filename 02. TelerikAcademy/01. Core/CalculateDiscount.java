package telerikAcademy;

import java.util.Scanner;

public class CalculateDiscount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        double[] newPrices = new double[n];
        for (int i = 0; i < n; i++) {
            double price = Double.parseDouble(scanner.nextLine());
            newPrices[i] = price *0.35;
        }
        for (double price:newPrices) {
            System.out.printf("%.2f%n",price);
        }
    }
}

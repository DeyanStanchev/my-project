package telerikAcademy;

import java.util.Scanner;

public class DeckOfCards {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String sign = scanner.nextLine();
        if (Character.isDigit(sign.charAt(0))) {
            int number = Integer.parseInt(sign);
            for (int i = 2; i <= number; i++) {
                System.out.printf("%d of spades, %d of clubs, %d of hearts, %d of diamonds%n", i, i, i, i);
            }
        } else {
            switch (sign) {
                case "J":
                    for (int i = 2; i <= 11; i++) {
                        if (i == 11) {
                            System.out.println("J of spades, J of clubs, J of hearts, J of diamonds");
                        } else {
                            System.out.printf("%d of spades, %d of clubs, %d of hearts, %d of diamonds%n", i, i, i, i);
                        }
                    }
                    break;
                case "Q":
                    for (int i = 2; i <= 12; i++) {
                        if (i == 11) {
                            System.out.println("J of spades, J of clubs, J of hearts, J of diamonds");
                        } else if (i == 12) {
                            System.out.println("Q of spades, Q of clubs, Q of hearts, Q of diamonds");
                        } else {
                            System.out.printf("%d of spades, %d of clubs, %d of hearts, %d of diamonds%n", i, i, i, i);
                        }
                    }
                    break;
                case "K":
                    for (int i = 2; i <= 13; i++) {
                        if (i == 11) {
                            System.out.println("J of spades, J of clubs, J of hearts, J of diamonds");
                        } else if (i == 12) {
                            System.out.println("Q of spades, Q of clubs, Q of hearts, Q of diamonds");
                        } else if (i == 13) {
                            System.out.println("K of spades, K of clubs, K of hearts, K of diamonds");
                        } else {
                            System.out.printf("%d of spades, %d of clubs, %d of hearts, %d of diamonds%n", i, i, i, i);
                        }
                    }
                    break;
                case "A":
                    for (int i = 2; i <= 14; i++) {
                        if (i == 11) {
                            System.out.println("J of spades, J of clubs, J of hearts, J of diamonds");
                        } else if (i == 12) {
                            System.out.println("Q of spades, Q of clubs, Q of hearts, Q of diamonds");
                        } else if (i == 13) {
                            System.out.println("K of spades, K of clubs, K of hearts, K of diamonds");
                        } else if (i == 14) {
                            System.out.println("A of spades, A of clubs, A of hearts, A of diamonds");
                        } else {
                            System.out.printf("%d of spades, %d of clubs, %d of hearts, %d of diamonds%n", i, i, i, i);
                        }
                    }
                    break;
                default:
                    System.out.println("Invalid card");
                    break;
            }
        }
    }
}

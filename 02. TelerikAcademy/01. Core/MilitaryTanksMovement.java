package telerikAcademy;

import java.util.Scanner;

public class MilitaryTanksMovement {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String coordinates = scanner.nextLine();
        int x = 0;
        int y = 0;
        for (int i = 0; i < coordinates.length(); i++) {
            switch(coordinates.charAt(i)){
                case 'U':
                    y++;
                    break;
                case 'D':
                    y--;
                    break;
                case 'L':
                    x--;
                    break;
                case 'R':
                    x++;
                    break;
                default:
                    System.out.println("Invalid operation");
                    break;
            }
        }
        System.out.printf("(%d, %d)",x,y);
    }
}

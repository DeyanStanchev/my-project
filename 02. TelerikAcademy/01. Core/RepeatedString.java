package hackerRank;

import java.util.ArrayList;
import java.util.Scanner;

public class RepeatedString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String repeated = scanner.nextLine();
        String s = "hello";
        char some = "asdfg".charAt(3);
        System.out.println(some);
        long count = Long.parseLong(scanner.nextLine());
        long getit = countTimes(repeated, count);
        //System.out.println(getit);
    }

    static long countTimes(String s, long n) {
        long numOfS = n / s.length();
        long rest = n % s.length();

        if (!s.contains("a")) return 0;
        return s.length() > n ? aCounter(s, rest)
                : numOfS * aCounter(s, s.length()) + aCounter(s, rest);
    }

    private static long aCounter(String s, long end) {
        int a = 0;
        for (int i = 0; i < end; i++) {
            if (s.charAt(i) == 'a') a++;
        }
        return a;
    }
}

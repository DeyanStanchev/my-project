package hackerRank;

import javax.management.RuntimeErrorException;
import java.util.Scanner;

public class IPRegex {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String adress = scanner.nextLine();
        while (!adress.isEmpty()) {
            String[] parts = adress.split("[.]");
            boolean valid = true;
            for (int i = 0; i < parts.length; i++) {
                String tmp = parts[i];
                try {
                    int tmpNum = Integer.parseInt(tmp);
                    if (tmpNum > 255 || tmpNum < 0) {
                        valid = false;
                        break;
                    }
                } catch (NumberFormatException n) {
                    valid = false;
                }
                if (tmp.length() > 3) {
                    valid = false;
                    break;
                }
            }
            if (parts.length != 4) {
                valid = false;
            }
            System.out.println(valid);
            adress = scanner.nextLine();
        }
    }
}

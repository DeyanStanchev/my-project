package telerikAcademy;

import java.util.Scanner;

public class PhoneBill {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int messages = Integer.parseInt(scanner.nextLine());
        int minutes = Integer.parseInt(scanner.nextLine());
        int addMsgs = messages-20;
        int addMins = minutes-60;
        double sum = 0;
        double addMsgsTax = 0;
        double addMsgsPrice = 0;
        double addMinsTax = 0;
        double addMinsPrice = 0;

        if(addMsgs>0){
            addMsgsPrice = addMsgs*0.06;
            addMsgsTax = addMsgsPrice*0.2;
        }else{
            addMsgs=0;
        }
        if(addMins>0){
            addMinsPrice = addMins*0.1;
            addMinsTax = addMinsPrice*0.2;
        }else{
            addMins=0;
        }

        double totalSum = 12+addMinsPrice+addMinsTax+addMsgsPrice+addMsgsTax;

        System.out.printf("%d additional messages for %.2f levas%n",addMsgs,addMsgsPrice);
        System.out.printf("%d additional messages for %.2f levas%n",addMins,addMinsPrice);
        System.out.printf("%.2f additional taxes%n",addMinsTax+addMsgsTax);
        System.out.printf("%.2f total bill",totalSum);

    }
}

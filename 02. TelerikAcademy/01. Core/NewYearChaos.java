package hackerRank;

import java.util.ArrayList;
import java.util.Scanner;

public class NewYearChaos {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int testCases = scanner.nextInt();
        for (int i = 0; i < testCases; i++) {
            int people = scanner.nextInt();
            ArrayList<Integer> finalQueue = fillList(scanner,people);
            minimumBribes(finalQueue);
        }
    }

    private static void minimumBribes(ArrayList<Integer> finalQueue) {
        ArrayList<Integer> originalQueue = new ArrayList<>();
        for (int i = 1; i <= finalQueue.size(); i++) {
            originalQueue.add(i);
        }
        int bribes = 0;
        boolean isChaotic = false;
        for (int i = 1; i <= originalQueue.size(); i++) {
            int originalIndex = originalQueue.indexOf(i);
            int finalIndex = finalQueue.indexOf(i);
            int currentBribes = 0;
            if (originalIndex > finalIndex) {
                if (Math.abs(originalIndex - finalIndex) == 1) {
                    currentBribes += 1;
                } else if (Math.abs(originalIndex - finalIndex) == 2) {
                    currentBribes += 2;
                } else {
                    currentBribes = -1;
                }
                if (currentBribes != -1) {
                    bribes += currentBribes;
                } else {
                    isChaotic = true;
                    break;
                }
            }
        }
        if (isChaotic) {
            System.out.println("Too chaotic");
        } else {
            System.out.println(bribes);
        }
    }

    private static ArrayList<Integer> fillList(Scanner scanner,int length) {
        ArrayList<Integer> array = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            array.add(scanner.nextInt());
        }
        return array;
    }
}

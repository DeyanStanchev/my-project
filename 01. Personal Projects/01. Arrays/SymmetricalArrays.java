package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class SymmetricalArrays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arraysToCome = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < arraysToCome; i++) {
            String[] tmp = fillArray(scanner);
            boolean symmetrical = checkSymmetrical(tmp);
            if(symmetrical){
                System.out.println("Yes");
            }else{
                System.out.println("No");
            }
        }
    }

    private static boolean checkSymmetrical(String[] tmp) {
        for (int i = 0; i < tmp.length/2; i++) {
            if(tmp[i].equals(tmp[tmp.length-1-i])){
                continue;
            }else{
                return false;
            }
        }
        return true;
    }

    private static String[] fillArray(Scanner scanner) {
        String line = scanner.nextLine();
        String[] lineArray = line.split(" ");
        return lineArray;
    }
}

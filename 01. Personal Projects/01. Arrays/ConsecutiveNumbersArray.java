package arraysExercises;

public class ConsecutiveNumbersArray {
    public static void main(String[] args) {

        int[] array = {1, 1, 3, 4, 5, 6, 7, 8, 9, 2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 3, 4, 5, 6, 7, 8};

        int counter = 0;
        int bestRow = 0;
        int bestLength = 0;
        int tempCount = 0;
        int tempLength = 0;

        for (int index = 0; index < array.length - 1; index++) {
            if ((array[index] + 1) == array[index + 1]) {
                tempCount++;
                tempLength++;
                if ((array[index] + 1) == array[index + 1] && tempCount > counter && tempLength > bestLength) {
                    bestRow = index - tempLength + 1;
                }
                if (tempCount > counter) {
                    counter = tempCount;
                    bestLength = tempLength;
                }
            }
            if ((array[index] + 1) != array[index + 1]) {
                tempCount = 0;
                tempLength = 0;
            }
        }
        for (int i = bestRow; i <= bestRow + bestLength; i++) {
            System.out.print(array[i] + " ");
        }
    }
}






package arraysExercises;

import java.util.Scanner;

public class IdenticalArrays {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the length of the Arrays");

        int n = scanner.nextInt();

        int[] myArray = new int[n];
        int[] myArray2 = new int[n];

        boolean identical = true;

        System.out.println("Please enter the numbers of your first Array");

        for (int index = 0; index < n; index++) {
            myArray[index] = scanner.nextInt();
        }

        System.out.println("Please enter the numbers of your first Array");

        for (int index = 0; index < n; index++) {
            myArray2[index] = scanner.nextInt();
        }
        for (int index = 0; index < n; index++) {
            if (myArray[index] != myArray2[index]) {
                identical = false;
            }
        }
        System.out.println("Are your arrays identical? "+identical);
    }
}

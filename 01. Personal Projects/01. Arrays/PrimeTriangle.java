package arrays;

import java.util.Scanner;

public class PrimeTriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        for (int i = 1; i <= n; i++) {
            boolean isPrime = true;
            for (int j = 2; j <= i / 2; j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                for (int j = 1; j <= i; j++) {
                    boolean isItPrime = true;
                    for (int k = 2; k <= j / 2; k++) {
                        if (j % k == 0) {
                            isItPrime = false;
                            break;
                        }
                    }
                    if (isItPrime) {
                        System.out.print(1);
                    } else {
                        System.out.print(0);
                    }
                }
                System.out.println();
            }
        }
    }
}

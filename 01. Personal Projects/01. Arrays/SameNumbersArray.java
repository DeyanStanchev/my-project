package arraysExercises;

public class SameNumbersArray {

    public static void main(String[] args) {

        int[] array = {2, 2, 1, 2, 3, 3, 3, 3, 3, 3, 2, 2, 2, 1, 1};

        int counter = 0;
        int bestRow = 0;
        int bestLength = 0;
        int tempCount = 0;
        int tempLength = 0;


        for (int index = 0; index < (array.length - 1); index++) {

            if (array[index] == array[index + 1]) {
                tempCount++;
                tempLength++;
                if (array[index] == array[index + 1] && tempCount > counter && tempLength > bestLength) {
                    bestRow = index - tempLength + 1;
                }
                if (tempCount > counter) {
                    counter = tempCount;
                    bestLength = tempLength;
                }
            }
            if (array[index] != array[index + 1]) {
                tempCount = 0;
                tempLength = 0;
            }
        }
        for (int i = bestRow; i <= bestRow + bestLength; i++) {
            System.out.print(array[i] + " ");

        }
    }
}














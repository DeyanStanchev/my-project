package arrays;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Scanner;

public class AboveTheMainDiagonal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        long[][] matrix = new long[n][n];
        BigInteger sum = new BigInteger("0");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (i == 0 && j == 0) {
                    matrix[i][j] = 1;
                    continue;
                } else if (j == 0) {
                    matrix[i][j] = matrix[i - 1][j] * 2;
                    continue;
                }
                matrix[i][j] = matrix[i][j - 1] * 2;
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (i == j) {
                    continue;
                }
                if (j > i) {
                    sum = sum.add(BigInteger.valueOf(matrix[i][j]));
                }
            }
        }
        System.out.println(sum);
    }
}

package arraysExercises;

import java.util.Scanner;

public class MostFrequentElement {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        int[] array = new int[n];
        int counter = 0;
        int bestNum = 0;

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        for (int i = 0; i < array.length - 1; i++) {
            int tempCounter = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j]) {
                    tempCounter++;
                }
                if (tempCounter > counter) {
                    bestNum = array[i];
                    counter = tempCounter;
                }
            }
        }
        System.out.println("The moest frequent number is: " + bestNum + " it is repeated " + counter + " times.");
    }
}

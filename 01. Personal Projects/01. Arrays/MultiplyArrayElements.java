package arrays;

import java.util.Scanner;

public class MultiplyArrayElements {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());

        int[] array = new int [n];

        for (int index = 0; index < n; index++) {
            array[index]=Integer.parseInt(scanner.next());
        }

        for (int index = 0; index < array.length; index++) {
            array[index]=2*array[index];
            System.out.print(array[index]+" ");
        }

    }
}

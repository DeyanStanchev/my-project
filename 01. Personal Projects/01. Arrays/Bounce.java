package arrays;

import java.util.Scanner;

public class Bounce {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        long[][] matrix = new long[n][m];
        long sum = 0;


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = (long) Math.pow(2, i + j);
            }
        }
        int i = 0;
        int j = 0;
        String direction = "SE";
        while (i >= 0 && i < n && j >= 0 && j < m) {
            switch (direction) {
                case "SE":
                    sum += matrix[i++][j++];
                    break;
                case "NE":
                    sum += matrix[i--][j++];
                    break;
                case "NW":
                    sum += matrix[i--][j--];
                    break;
                case "SW":
                    sum += matrix[i++][j--];
                    break;
            }
            if (i == n - 1 && j != 0) {
                direction = "NE";
            } else if (j == m - 1) {
                direction = "NW";
            } else if (i == 0 && j < m - 1) {
                direction = "SW";
            }else if( j == 0 && i < n-1){
                direction = "SE";
            }
        }
        System.out.println(sum);
    }
}

package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BigNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int firstSize = scanner.nextInt();
        int secondSize = scanner.nextInt();
        int maxSize = Math.max(firstSize,secondSize);
        scanner.nextLine();
        long[] firstArray = fillArray(scanner, maxSize);
        long[] secondArray = fillArray(scanner,maxSize);
        long[] summedArrays = sumArrays(firstArray, secondArray);
        for (long summedArray : summedArrays) {
            System.out.print(summedArray+" ");
        }

    }

    private static long[] sumArrays(long[] firstArray, long[] secondArray) {
        long[] result = new long[firstArray.length];
        for (int i = 0; i < Math.max(firstArray.length,secondArray.length); i++) {
            long sum = firstArray[i] + secondArray[i];
            if (sum >= 10) {
                long digit = sum % 10;
                firstArray[i+1] = firstArray[i+1] +1;
                result[i]=digit;
            }else{
                result[i]=sum;
            }
        }
        return result;
    }

    private static long[] fillArray(Scanner scanner, int n) {
        long[] array = new long[n];
        String[] firstLine = scanner.nextLine().split(" ");
        for (int i = 0; i < n; i++) {
            if(i>firstLine.length-1){
                break;
            }
            array[i] = Integer.parseInt(firstLine[i]);
        }
        return array;
    }
}

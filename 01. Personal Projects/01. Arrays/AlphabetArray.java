package arraysExercises;

import java.util.Scanner;

public class AlphabetArray {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a word.");
        String word = scanner.nextLine();

        char[] alpha = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        int length = word.length();

        for (int i = 0; i < word.length(); i++) {
            char letter = word.charAt(i);
            for (int j = 0; j < alpha.length; j++) {
                if (alpha[j] == letter) {
                    System.out.printf("The index for %s  is: %d%n", letter, j);
                }
            }
        }
    }
}

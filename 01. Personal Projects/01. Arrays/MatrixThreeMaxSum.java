package arraysExercises;

import java.util.Scanner;

public class MatrixThreeMaxSum {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the size of the matrix.");
        int n = Integer.parseInt(scanner.nextLine());
        int m = Integer.parseInt(scanner.nextLine());

        int[][] matrix = new int[n][m];

        int maxSum = Integer.MIN_VALUE;
        int sum = 0;
        int bestRow = 0;
        int bestCol = 0;

        System.out.println("Please enter the elements of the matrix: " + m + " numbers on " + n + " rows.");

        for (int rows = 0; rows < n; rows++) {
            for (int cols = 0; cols < m; cols++) {
                matrix[rows][cols] = scanner.nextInt();
            }
        }
        for (int rows = 0; rows < matrix.length-2; rows++) {
            for (int cols = 0; cols < matrix[0].length-2; cols++) {

                sum = matrix[rows][cols]+matrix[rows][cols+1]+matrix[rows][cols+2]+matrix[rows+1][cols]+
                        matrix[rows+1][cols+1]+matrix[rows+1][cols+2]+matrix[rows+2][cols]+matrix[rows+2][cols+1]+
                        matrix[rows+2][cols+2];
                if ( sum > maxSum){
                    maxSum = sum;
                    bestRow = rows;
                    bestCol = cols;
                }

            }
        }
        System.out.println("The best platform is: ");
        System.out.printf("  %d %d %d%n",matrix[bestRow][bestCol],matrix[bestRow][bestCol+1],matrix[bestRow][bestCol+2]);
        System.out.printf("  %d %d %d%n",matrix[bestRow+1][bestCol],matrix[bestRow+1][bestCol+1],matrix[bestRow+1][bestCol+2]);
        System.out.printf("  %d %d %d%n",matrix[bestRow+2][bestCol],matrix[bestRow+2][bestCol+1],matrix[bestRow+2][bestCol+2]);
        System.out.printf("The maximum sum is: %d%n",sum);
    }
}

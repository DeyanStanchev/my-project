package arraysExercises;

import java.util.Scanner;
import java.util.Arrays;

public class SelectionSortArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        int[] array = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i]= scanner.nextInt();
        }
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                int switcher = 0;
                if (array[j] > array[i]) {
                    switcher =  array[j];
                    array[j]=array[i];
                    array[i]=switcher;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}

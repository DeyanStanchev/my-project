package arraysExercises;

import java.util.Arrays;

public class AlphabeticalOrderArrays {
    public static void main(String[] args) {

        char[] array = {'b', 'd', 'a', 'r'};
        char[] arrayTwo = {'b', 'd', 'a','b'};
        boolean none = false;

        for (int i = 0; i < arrayTwo.length; i++) {
            if (array[i] == arrayTwo[i]) {
                continue;
            }
            if (array[i] < arrayTwo[i]) {
                none=true;
                System.out.println(Arrays.toString(array));
                break;
            } else {
                none=true;
                System.out.println(Arrays.toString(arrayTwo));
                break;
            }
        }
        if (!none) {
            System.out.println(Arrays.toString(arrayTwo));
        }
    }
}







package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class ThreeGroups {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] line = scanner.nextLine().split(" ");
        ArrayList<Integer> firstLine = new ArrayList<>();
        ArrayList<Integer> secondLine = new ArrayList<>();
        ArrayList<Integer> thirdLine = new ArrayList<>();

        for (int i = 0; i < line.length; i++) {
            int tmp = Integer.parseInt(line[i]);
            if (tmp % 3 == 0) {
                firstLine.add(tmp);
            } else if (tmp % 3 == 1) {
                secondLine.add(tmp);
            } else if (tmp % 3 == 2) {
                thirdLine.add(tmp);
            }
        }
        printArray(firstLine);
        printArray(secondLine);
        printArray(thirdLine);
    }

    private static void printArray(ArrayList<Integer> line) {
        for (Integer integer : line) {
            System.out.print(integer + " ");
        }
        System.out.println();
    }
}

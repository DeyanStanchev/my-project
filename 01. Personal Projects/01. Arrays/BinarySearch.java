package arraysExercises;

import java.util.Scanner;

public class BinarySearch {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        System.out.println("Please enter a number from 1 to 15.");

        int x = Integer.parseInt(scanner.nextLine());;
        int left = 0;
        int right = array.length-1;
        boolean found = false;
        while(left<=right){
            int mid = left+((right-left)/2);
            if ( array[mid]== x ){
                System.out.printf("Your number is in the array at index: %d",mid);
                found = true;
                break;
            } else if ( x > mid){
                left = mid+1;
            }else {
                right = mid-1;
            }
        }
        if (!found){
            System.out.println("Your number was not found in the array.");
        }
    }
}

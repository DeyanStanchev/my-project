package arrays;

import java.util.Scanner;

public class LongestSequenceOfEqual {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[] numbers = new int[n];
        int counter = 1;
        int bestSequence = 0;
        for (int i = 0; i < n; i++) {
            numbers[i] = Integer.parseInt(scanner.nextLine());
        }
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i]==numbers[i-1]){
                counter ++;
            }else{
                counter = 1;
            }
            if (counter > bestSequence){
                bestSequence = counter;
            }
        }
        System.out.println(bestSequence);
    }
}

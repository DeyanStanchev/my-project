package arrays;

import java.util.Scanner;

public class LongestIncreasingSequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[] array = readArray(scanner,n);
        int longest = findLongest(array);
        System.out.println(longest);
    }

    private static int findLongest(int[] array) {
        int longestSequence=1;
        int bestSequence=1;
        for (int i = 0; i < array.length-1; i++) {
            if(array[i]<array[i+1]){
                longestSequence++;
            }else{
                longestSequence=1;
            }
            if(longestSequence>bestSequence){
                bestSequence=longestSequence;
            }

        }

        return bestSequence;
    }

    private static int[] readArray(Scanner scanner,int n) {
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i]=Integer.parseInt(scanner.nextLine());
        }
        return array;
    }
}

package arraysExercises;

import java.util.Scanner;

public class TwoDimentionalArray1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        int[][] matrix = new int[n][n];
        int a = 1;

        for (int row = 0; row < n; row++) {
            int b = a ;
            for (int col = 0; col < n; col++) {
                matrix[row][col] = b;
                System.out.printf("%d ", matrix[row][col]);
                b+=n;
            }
            System.out.println();
            a++;
        }
    }
}




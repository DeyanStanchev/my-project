package methods;

import java.util.Scanner;

public class ZigZag {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(" ");
        int n = Integer.parseInt(input[0]);
        int m = Integer.parseInt(input[1]);
        long sum = 0;
        for (int i = 0; i < n; i++) {
            sum += sumRow(i, m, n);
        }
        System.out.println(sum);
    }

    public static long sumRow(int row, int cols, int rows) {
        long sum = 0;
        if (row % 2 == 0) {
            for (int j = 0; j < cols; j = j + 2) {
                sum += 1 + (row + j) * 3;
                if (row > 0 && row < rows - 1 && j > 0 && j < cols - 1)
                    sum += 1 + (row + j) * 3;
            }
        } else
            for (int j = 1; j < cols; j = j + 2) {
                sum += 1 + (row + j) * 3;
                if (row > 0 && row < rows - 1 && j > 0 && j < cols - 1)
                    sum += 1 + (row + j) * 3;
            }
        return sum;
    }
}
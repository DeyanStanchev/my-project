package methodsExercises;

import java.util.Scanner;

public class LastDigitInWord {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int digit = getLastDigit(scanner);
        String digitName = convertToString(digit);
        printDigitName(digitName);


    }
    public static int getLastDigit(Scanner scanner){
        int number=scanner.nextInt();
        int digit = number%10;
        return digit;
    }
    public static String convertToString( int digit){
        String digitName = "";
        switch (digit){
            case 0:
                digitName = "Zero";
                break;
            case 1:
                digitName = "One";
                break;
            case 2:
                digitName = "Two";
                break;
            case 3:
                digitName = "Three";
                break;
            case 4:
                digitName = "Four";
                break;
            case 5:
                digitName = "Five";
                break;
            case 6:
                digitName = "Six";
                break;
            case 7:
                digitName = "Seven";
                break;
            case 8:
                digitName = "Eight";
                break;
            case 9:
                digitName = "Nine";
                break;
            default:
                digitName = "Error!";
                break;
        }
        return digitName;

    }
    public static void printDigitName ( String digitName){
        System.out.println("The last digit in your number is: "+digitName);

    }
}

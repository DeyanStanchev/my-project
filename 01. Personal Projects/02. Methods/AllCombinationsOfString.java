package recurssion;

import java.util.Scanner;

public class AllCombinationsOfString {
    //public static int variations;
    public static String[] inputWords;
    public static String[] sortedWords;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter how many words followed by the words on a new line");
        inputWords = getArray(scanner);
        //System.out.print("Number of words in each combination: ");
        //variations = scanner.nextInt();
        scanner.close();
        sortedWords = new String[inputWords.length];
        for (int i = 0; i <= inputWords.length ; i++) {
            wordsCombinations(0,0,i);

        }
    }
    public static void wordsCombinations(int currentRow,int number,int variations){
        if ( currentRow >= variations){
            printLoops(variations);
            return;
        }
        for (int i = number; i < inputWords.length; i++) {
            sortedWords[currentRow] = inputWords[i];
            wordsCombinations(currentRow+1,i+1,variations);
        }

    }
    public static void printLoops(int variations){
        for (int i = 0; i < variations ; i++) {
            System.out.printf("%s ", sortedWords[i]);
        }
        System.out.println();

    }

    public static String[] getArray(Scanner scanner) {
        int length = Integer.parseInt(scanner.nextLine());
        String[] words = new String[length];
        for (int i = 0; i < words.length; i++) {
            words[i] = scanner.next();
        }
        return words;
    }
}

package methods;

public class SortingNumbers {
    public static void main(String[] args) {


        int[] numbers = sortNums(1, 3, 32, -2, 4, 13, 5);
        printNumbers(numbers);


    }

    public static int[] sortNums(int... numbers) {
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[i] > numbers[j]) {
                    int tempVar = numbers[i];
                    numbers[i] = numbers[j];
                    numbers[j] = tempVar;
                }
            }
        }
        return numbers;
    }

    public static void printNumbers(int... numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.printf("%d", numbers[i]);
            if (i < (numbers.length - 1)) {
                System.out.print(",");
            }
        }
    }
}

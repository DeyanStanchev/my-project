package recurssion;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        long sum = fac(n);
        System.out.println(sum);
    }
    static long fac(int n){
        if(n<1){
            return 1;
        }
        return n*fac(n-1);
    }
}



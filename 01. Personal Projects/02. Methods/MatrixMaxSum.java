package methods;

import java.util.Scanner;

public class MatrixMaxSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[][] matrix = readMatrix(scanner, n);
        int[] coordinates = readCoordinates(scanner);
        long maxSum = -Long.MAX_VALUE;
        for (int i = 0; i < coordinates.length - 1; i += 2) {
            int row = coordinates[i];
            int col = coordinates[i + 1];
            long tmpSum = 0;
            if (row > 0) {
                if (col > 0) {
                    row--;
                    col--;
                    for (int j = 0; j <= col; j++) {
                        tmpSum += matrix[row][j];
                    }
                    for (int j = row - 1; j >= 0; j--) {
                        tmpSum += matrix[j][col];
                    }
                } else {
                    row--;
                    col++;
                    for (int j = 0; j <= Math.abs(col); j++) {
                        tmpSum += matrix[row][j];
                    }
                    for (int j = row + 1; j < matrix.length; j++) {
                        tmpSum += matrix[j][Math.abs(col)];
                    }
                }
            } else {
                if (col > 0) {
                    row++;
                    col--;
                    for (int j = matrix[0].length - 1; j >= col; j--) {
                        tmpSum += matrix[Math.abs(row)][j];
                    }
                    for (int j = Math.abs(row)-1; j >= 0; j--) {
                        tmpSum += matrix[j][col];
                    }
                } else {
                    row++;
                    col++;
                    for (int j = matrix[0].length - 1; j >= Math.abs(col); j--) {
                        tmpSum += matrix[Math.abs(row)][j];
                    }
                    for (int j = Math.abs(row) + 1; j < matrix.length; j++) {
                        tmpSum += matrix[j][Math.abs(col)];
                    }
                }
            }
            if (tmpSum >= maxSum) {
                maxSum = tmpSum;
            }
        }
        System.out.println(maxSum);
    }

    private static int[] readCoordinates(Scanner scanner) {
        String[] coordinates = scanner.nextLine().split(" ");
        int[] converted = new int[coordinates.length];
        for (int i = 0; i < converted.length; i++) {
            converted[i] = Integer.parseInt(coordinates[i]);
        }
        return converted;
    }

    private static int[][] readMatrix(Scanner scanner, int n) {
        String line = scanner.nextLine();
        String[] numbers = line.split(" ");
        int[][] matrix = new int[n][numbers.length];
        for (int i = 0; i < n; i++) {
            numbers = line.split(" ");
            for (int j = 0; j < numbers.length; j++) {
                matrix[i][j] = Integer.parseInt(numbers[j]);
            }
            if (i < n - 1) {
                line = scanner.nextLine();
            }
        }
        return matrix;
    }
}

package methodsExercises;

import java.util.Scanner;

public class DigitFoundInArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the number of elements in your array.");
        int numbers = Integer.parseInt(scanner.nextLine());
        System.out.println("Please enter the elements of your array.");
        int[] array = readNumbers(numbers,scanner);
        System.out.println("Please enter which digit would you like to search.");
        int digit = scanner.nextInt();
        int counterDigit = timesDigitFound(digit,array);
        printCounter(counterDigit,digit);

    }
    public static int[] readNumbers (int numbers,Scanner scanner){
        int[] array = new int[numbers];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }
    public static int timesDigitFound(int digit,int...numbers){
        int[] array = numbers;
        int counterDigit = 0;
        for (int i = 0; i <array.length ; i++) {
            if ( array[i]==digit){
                counterDigit++;
            }
        }
        return counterDigit;
    }
    public static void printCounter (int counterDigit,int digit){
        System.out.printf("Your number %d is found %d time(s) in your array.",digit,counterDigit);
    }
}

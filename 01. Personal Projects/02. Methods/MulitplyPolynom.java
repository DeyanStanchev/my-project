package methodsExercises;

import java.util.Scanner;
import java.util.Arrays;

public class MulitplyPolynom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the number of elements of your first Polynom.");
        int first = scanner.nextInt();
        int[] firstPolynom = readArray(scanner, first);
        System.out.println("Please enter the number of elements of your second Polynom.");
        int second = scanner.nextInt();
        int[] secondPolynom = readArray(scanner, second);
        int[] multipliedPolynom = multiplyPolynom(firstPolynom,secondPolynom);
        int sum = sumOfPolynom(multipliedPolynom);
        System.out.println("Your multiplied polynoms are equla to: "+ Arrays.toString(multipliedPolynom));
        System.out.println("Your sum is: "+sum);

    }
    public static int sumOfPolynom(int...numbers){
        int sum=0;
        for (int i = 0; i < numbers.length; i++) {
            sum+=numbers[i];
        }
        return sum;
    }
    public static int[] multiplyPolynom(int[] firstPolynom,int[] secondPolynom){
        int[] multipliedPolynom = new int[(firstPolynom.length*secondPolynom.length)];
        int index = 0;
        for (int k : firstPolynom) {
            for (int i : secondPolynom) {
                multipliedPolynom[index] = k * i;
                index++;
            }
        }
        return multipliedPolynom;
    }

    public static int[] readArray(Scanner scanner, int number) {
        System.out.println("Please enter the elements of your Array.");
        int[] array = new int[number];
        for (int i = 0; i < array.length; i++) {
            int element = scanner.nextInt();
            array[i] = element;
        }
        return array;
    }
}

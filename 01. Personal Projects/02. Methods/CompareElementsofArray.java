package methodsExercises;

import java.util.Scanner;

public class CompareElementsofArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the number of elements in your Array.");
        int numbers = scanner.nextInt();
        System.out.println("Please enter your Array.");
        int[] array = readArray(numbers, scanner);
        System.out.println("Please enter the index which should be compared.");
        int index = scanner.nextInt();

        String next = compareNextElement(index, array);
        String previous = comparePreviousElement(index, array);
        comparison(previous, next);
    }

    public static int[] readArray(int numbers, Scanner scanner) {
        int[] array = new int[numbers];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    public static String compareNextElement(int index, int... numbers) {
        String compared = "";
        if (index < numbers.length - 1) {
            if (numbers[index] > numbers[index + 1]) {
                compared = "bigger than the next one.";
            } else {
                compared = "smaller than the next one.";
            }
        } else {
            compared = "is the last one.";
        }
        return compared;
    }

    public static String comparePreviousElement(int index, int... numbers) {
        String compared = "";
            if (numbers[index] > numbers[index - 1]) {
                compared = "is bigger than the previous one.";
            } else {
                compared = "is smaller than the previous one.";
            }
        return compared;
        }

    public static void comparison(String previous, String next) {
        System.out.printf("The element is %s%n", next);
        System.out.printf("The element is %s%n", previous);
    }
}

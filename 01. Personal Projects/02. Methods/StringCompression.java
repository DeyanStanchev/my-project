package stringManipulationExercises;

public class StringCompression {
    public static void main(String[] args) {

        String letters = "aaaabbbccccd";
        int counter = 1;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < letters.length(); i++) {
            if (i != letters.length() - 1) {
                if (letters.charAt(i) == letters.charAt(i + 1)) {
                    counter++;
                    continue;
                }
                if (counter > 1) {
                    sb.append(letters.charAt(i)).append(counter);
                    counter = 1;
                } else {
                    sb.append(letters.charAt(i));
                }
            } else if (counter > 1) {
                sb.append(letters.charAt(i)).append(counter);
            } else {
                sb.append(letters.charAt(i));
            }
        }
        System.out.println(sb.toString());
    }
}


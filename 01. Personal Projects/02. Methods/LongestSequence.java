package methods;

import java.util.Scanner;

public class LongestSequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numberOfNumbers = Integer.parseInt(scanner.nextLine());
        int[] numbers = new int[numberOfNumbers];

        for (int i = 0; i < numberOfNumbers; i++) {
            numbers[i] = Integer.parseInt(scanner.nextLine());
        }
        int best = 0;
        int counter = 1;

        for (int i = 0; i < numbers.length - 1; i++) {
            if(numbers[i]==numbers[i+1]){
                counter++;
            }else{
                counter=1;
            }
            if(counter>best){
                best=counter;
            }
        }
        System.out.println(best);
    }
}

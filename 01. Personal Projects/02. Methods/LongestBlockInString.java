package methods;

import java.util.Scanner;

public class LongestBlockInString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String line = scanner.nextLine();
        int currentPsn = 0;
        int currentLength = 1;
        int bestPsn = 0;
        int bestLength = 0;

        for (int i = 1; i < line.length(); i++) {
            if (line.charAt(i) == line.charAt(i -1)) {
                currentLength++;
                if (currentLength > bestLength) {
                    bestLength = currentLength;
                    bestPsn = (i-bestLength)+1;
                }
            } else {
                currentLength = 1;
            }
        }
        if(bestLength==0){
            System.out.println(line.charAt(0));
        }
        String result = line.substring(bestPsn,bestPsn+bestLength);
        System.out.println(result);
    }
}

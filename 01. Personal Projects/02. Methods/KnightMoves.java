package methods;

import java.util.Scanner;

public class KnightMoves {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int counter = 1;
        int[][] board = new int[n][n];
        int row = 0;
        int col = 0;
        int[] validMoves = {-2, -1, -2, 1, -1, -2, -1, 2, 1, -2, 1, 2, 2, -1, 2, 1};
        board[0][0] = counter;
        while (counter < n * n) {
            counter++;
            int[] move = isValidMove(row,col,board, validMoves);
            row = move[0];
            col = move[1];
            board[row][col] = counter;
        }
        for (int[] rows : board) {
            for (int element : rows) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }

    private static int[] isValidMove(int r, int c ,int[][] board, int[] validMoves) {
        int[] move = new int[2];
        boolean foundMove = false;
        for (int i = 0; i < validMoves.length - 1; i += 2) {
            int row = validMoves[i];
            int col = validMoves[i + 1];
            int tmpRow = r+row;
            int tmpCol = c+col;
            if (tmpRow >= 0 && tmpRow <= board.length - 1 && tmpCol >= 0 && tmpCol <= board.length - 1 && board[tmpRow][tmpCol] == 0) {
                move[0] = tmpRow;
                move[1] = tmpCol;
                return move;
            }
        }
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == 0) {
                    move[0] = i;
                    move[1] = j;
                    foundMove=true;
                    break;
                }
            }
            if (foundMove) {
                break;
            }
        }
        return move;
    }
}

package methods;

import java.util.Scanner;

public class TimeBetweenMonths {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int firstMonth = scanner.nextInt();
        int secondMonth = scanner.nextInt();
        printMonths(timeBetweenMonths(firstMonth,secondMonth),nameOfMonth(firstMonth),nameOfMonth(secondMonth));
    }
    public static void printMonths(int timeBetweenMonths , String firstMonth , String secondMonth){
        System.out.printf("There is a period of %d months from %s to %s.",timeBetweenMonths,firstMonth,secondMonth);
    }
    public static int timeBetweenMonths(int firstMonth , int secondMonth){
        int time = secondMonth - firstMonth;
        if ( time < 0){
            time = time +12;
        }
        return time;
    }
    public static String nameOfMonth(int month){
        String nameOfMonth ="";
        switch(month){
            case 1:
                nameOfMonth = "January";
                break;
            case 2:
                nameOfMonth = "February";
                break;
            case 3:
                nameOfMonth = "March";
                break;
            case 4:
                nameOfMonth = "April";
                break;
            case 5:
                nameOfMonth = "May";
                break;
            case 6:
                nameOfMonth = "June";
                break;
            case 7:
                nameOfMonth = "July";
                break;
            case 8:
                nameOfMonth = "August";
                break;
            case 9:
                nameOfMonth = "September";
                break;
            case 10:
                nameOfMonth = "October";
                break;
            case 11:
                nameOfMonth = "November";
                break;
            case 12:
                nameOfMonth = "December";
                break;
            default:
                nameOfMonth = "Error";
                break;
        }
        return nameOfMonth;
    }
}

package methods;

import java.util.Scanner;

public class NumberSign {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        numberSign(n);

    }
    public static void numberSign (int number){
        if(number>0){
            System.out.println("Positive");
        } else if ( number<0){
            System.out.println("Negative");
        } else {
            System.out.println("The number is 0");
        }
    }

}


package methods;

import java.util.Scanner;

public class MultiplyEvenWithOdd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numbers = Integer.parseInt(scanner.nextLine());
        int[] array = readNumbers(numbers, scanner);

        int evenSum = evenSum(array);
        int oddSum = oddSum(array);
        int result = result(evenSum, oddSum);

        printResult(evenSum,oddSum,result);
    }
    public static void printResult ( int evenSum , int oddSum , int result){
        System.out.printf("The sum of all even numbers is %d.%n",evenSum);
        System.out.printf("The sum of all odd numbers is %d.%n",oddSum);
        System.out.printf("The multiplication of even sum and odd sum is %d.",result);
    }
    public static int result ( int evenSum, int oddSum){
        return evenSum*oddSum;
    }

    public static int evenSum(int... numbers) {
        int sum = 0;
        for (int number : numbers) {
            if (number % 2 == 0) {
                sum += number;
            }
        }
        return sum;
    }

    public static int oddSum(int... numbers) {
        int sum1 = 0;
        for (int number : numbers) {
            if (number % 2 != 0) {
                sum1 += number;
            }
        }
        return sum1;
    }

    public static int multiplyEvenWithOdd(int evenSum, int oddSum) {
        return (evenSum*oddSum);

    }

    public static int[] readNumbers(int numbers,Scanner scanner) {
        int[] array = new int[numbers];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

}

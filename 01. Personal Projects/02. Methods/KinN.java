package recurssion;

import java.util.Scanner;

public class KinN {
    public static int nElements;
    public static int kElements;
    public static int[] array;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("N = ");
        nElements = scanner.nextInt();

        System.out.print("K = ");
        kElements = scanner.nextInt();
        scanner.close();

        array = new int[nElements];

        printVariations(0,1);
    }
    public static void printVariations(int currentLoop,int number) {
        if (currentLoop == nElements) {
            printLoops();
            return;
        }
        for (int i = number; i<= kElements; i++) {
            array[currentLoop] = i;
            printVariations(currentLoop+1,i);
        }
    }
    public static void printLoops(){
        for (int i = 0; i < nElements ; i++) {
            System.out.printf("%d ", array[i]);
        }
        System.out.println();
    }
}

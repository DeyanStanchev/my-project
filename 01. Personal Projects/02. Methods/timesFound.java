package stringManipulationExercises;

public class timesFound {
    public static void main(String[] args) {
        String text = "We are living in a yellow submarine. We don't have anything else. Inside the submarine is very tight. So we are drinking all the day. We will move out of it in 5 days.";
        int counter = 0;
        String textSmall = text.toLowerCase();
        int index = textSmall.indexOf("in");
        while(index!=-1){
            counter++;
            index = textSmall.indexOf("in",index+1);
        }
        System.out.println(counter);
    }
}

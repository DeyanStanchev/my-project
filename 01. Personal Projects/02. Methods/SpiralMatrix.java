package methods;

import java.util.Scanner;

public class SpiralMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        int[][] matrix = new int[n][n];

        int m = 1;
        int row1 = 0;
        int col1 = 0;
        int row2 = n - 1;
        int col2 = n - 1;

        while (m <= n * n ) {
            for (int i = col1; i <= col2; i++) {
                matrix[row1][i]=m++;
            }
            for (int i = row1+1; i <= row2; i++) {
                matrix[i][col2]=m++;
            }
            for (int i = col2-1; i>=col1 ; i--) {
                matrix[row2][i]=m++;
            }
            for (int i = row2-1; i >=row1+1 ; i--) {
                matrix[i][col1]=m++;
            }
            row1++;
            col1++;
            row2--;
            col2--;
        }
        printMatrix(matrix);
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}

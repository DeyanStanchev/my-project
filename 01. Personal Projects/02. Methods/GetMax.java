package methodsExercises;

import java.util.Scanner;

public class GetMax {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter 3 numbers: ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        int max = getMax(getMax(a,b),c);
        printMax(max);


    }
    public static int getMax( int a, int b){
        if ( a>b){
            return a;
        }
        return b;
    }
    public static void printMax(int a){
        System.out.println("The biggest number is: "+a);
    }
}

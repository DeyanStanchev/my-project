package methods;

import java.util.Scanner;
import java.util.regex.Pattern;

public class MaxEven {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        input= input.replaceAll("\\D+"," ");
        String[] numbers=input.split("[ .]+");


        long maxEven = -1;


        for (int i = 0; i < numbers.length; i++) {
            if (!numbers[i].isEmpty()) {
                int tmp = Integer.parseInt(numbers[i]);
                if (tmp % 2 == 0) {
                    if (tmp >= maxEven) {
                        maxEven = tmp;
                    }
                }
            }
        }

        if (maxEven < 0) {
            System.out.println(maxEven);
        } else {
            System.out.println(maxEven);
        }
    }
}

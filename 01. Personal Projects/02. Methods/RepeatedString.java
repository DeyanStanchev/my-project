package methods;

import java.util.Scanner;

public class RepeatedString {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        int repeatedCount = Integer.parseInt(scanner.nextLine());

        String repeated = repeatString(s,repeatedCount);
        System.out.println(repeated);
    }

    static String repeatString (String s , int repeatedCount){

        String[] stringArr = new String[repeatedCount];

        for (int i = 0; i < stringArr.length; i++) {
            stringArr[i] = s;
        }
        return String.join(",",stringArr);
    }
}

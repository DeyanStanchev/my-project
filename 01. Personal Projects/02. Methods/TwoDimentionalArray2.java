package arraysExercises;

import java.util.Scanner;

public class TwoDimentionalArray2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        int[][] matrix = new int[n][n];
        int a = 1;

        for (int col = 0; col < n; col++) {
            if (col == 0) {
                for (int row = 0; row < n; row++) {
                    matrix[row][col] = a;
                    a++;
                }
            } else if (col % 2 == 0) {
                for (int row = 0; row < n; row++) {
                    matrix[row][col] = a;
                    a++;
                }
            } else {
                for (int row = (n - 1); row >= 0; row--) {
                    matrix[row][col] = a;
                    a++;
                }
            }
        }
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                System.out.printf("%d ", matrix[row][col]);
            }
            System.out.println();
        }
    }
}


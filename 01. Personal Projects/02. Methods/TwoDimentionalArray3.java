package arraysExercises;

import java.util.Scanner;

public class TwoDimentionalArray3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        int[][] matrix = new int[n][n];
        int a = 1;

        for (int rows = n - 1; rows >= 0; rows--) {
            for (int cols = 0; cols < n; cols++) {
                if (matrix[rows][cols] == 0) {

                    if (Math.abs(rows - cols) == 3) {
                        matrix[rows][cols] = a;
                        a++;

                    }
                    if (rows - cols > 0 && Math.abs(rows - cols) == 2) {
                        matrix[Math.abs(rows - cols)][0] = a;
                        a++;
                        matrix[rows][cols] = a;
                        a++;
                    }
                    if (rows - cols < 0 && Math.abs(rows - cols) == 2) {
                        matrix[0][Math.abs(rows-cols)] = a;
                        a++;
                        matrix[rows][cols] = a;
                        a++;
                    }
                    if (rows-cols > 0 && Math.abs(rows - cols) == 1) {
                        matrix[Math.abs(rows - cols)][Math.abs(rows - cols)-1] = a;
                        a++;
                        matrix[Math.abs(rows - 1)][Math.abs(cols - 1)] = a;
                        a++;
                        matrix[rows][cols] = a;
                        a++;
                    }
                    if (rows-cols < 0 && Math.abs(rows - cols) == 1) {
                        matrix[Math.abs(rows - cols) - 1][Math.abs(rows - cols)] = a;
                        a++;
                        matrix[Math.abs(rows - 1)][Math.abs(cols - 1)] = a;
                        a++;
                        matrix[rows][cols] = a;
                        a++;
                    }
                    if (Math.abs(rows - cols) == 0) {
                        matrix[Math.abs(rows - cols)][Math.abs(rows - cols)] = a;
                        a++;
                        matrix[Math.abs(rows - 2)][Math.abs(cols - 2)] = a;
                        a++;
                        matrix[Math.abs(rows - 1)][Math.abs(cols - 1)] = a;
                        a++;
                        matrix[rows][cols] = a;
                        a++;
                    }
                }
            }
        }
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                System.out.printf("%d ", matrix[row][col]);
            }
            System.out.println();
        }
    }
}



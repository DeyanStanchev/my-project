package methods;

import java.util.Scanner;

public class MoveTwo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int startingPsn = Integer.parseInt(scanner.nextLine());
        int[] numbers = readArray(scanner);
        long sumFwd = 0;
        long sumBwds = 0;
        String move = scanner.nextLine();

        while (!move.equals("exit")) {
            String[] input = move.split(" ");
            int steps = Integer.parseInt(input[0]);
            String direction = input[1];
            int stepsSize = Integer.parseInt(input[2]);
            long[] tmpSum = calculateSumAndPsn(startingPsn, steps, stepsSize, direction, numbers);
            if (direction.equals("forward")) {
                sumFwd += tmpSum[0];
            } else {
                sumBwds += tmpSum[0];
            }
            startingPsn = (int) tmpSum[1];

            move = scanner.nextLine();
        }
        System.out.printf("Forward: %d%n", sumFwd);
        System.out.printf("Backwards: %d", sumBwds);

    }

    private static long[] calculateSumAndPsn(int startingPsn, int steps, int stepsSize, String direction, int[] numbers) {
        long[] array = new long[2];
        int current = 0;
        int tmpSum = 0;
        while (steps > 0) {
            switch (direction) {
                case "forward":
                    current = startingPsn + stepsSize;
                    if (current > numbers.length - 1) {
                        while(current>numbers.length-1) {
                            current = current - numbers.length;
                        }
                    }
                    tmpSum = numbers[current];
                    array[0] += tmpSum;
                    startingPsn = current;
                    break;
                case "backwards":
                    current = startingPsn - stepsSize;
                    if (current < 0) {
                        while(current<0) {
                            current = current + numbers.length;
                        }
                    }
                    tmpSum = numbers[current];
                    array[0] += tmpSum;
                    startingPsn = current;
                    break;
            }
            steps--;
        }
        array[1] = startingPsn;
        return array;
    }

    private static int[] readArray(Scanner scanner) {
        String line = scanner.nextLine();
        String[] numbers = line.split(",");
        int[] array = new int[numbers.length];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(numbers[i]);
        }
        return array;
    }
}

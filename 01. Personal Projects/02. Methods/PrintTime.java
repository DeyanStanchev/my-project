package methods;

import java.util.Scanner;

public class PrintTime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What time is it?");

        System.out.print("Hours: ");
        int hours = scanner.nextInt();

        System.out.print("Minutes: ");
        int minutes = scanner.nextInt();

        boolean validTime = validateHours(hours) && validateMinutes(minutes);

        if (validTime) {
            System.out.printf("The time is %d:%d now.%n", hours, minutes);
        } else {
            System.out.println("Invalid time!");
        }
    }

    public static boolean validateHours(int hours) {
        boolean result = (hours >= 0) && (hours < 24);
        return result;
    }

    public static boolean validateMinutes(int minutes) {
        boolean result = (minutes >= 0) && (minutes < 60);
        return result;
    }
}


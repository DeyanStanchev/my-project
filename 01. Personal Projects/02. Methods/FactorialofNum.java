package methodsExercises;

import java.util.Scanner;

public class FactorialofNum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number between 1 and 100");
        int num = scanner.nextInt();
        factorialNum(num);

    }

    static void factorialNum(int num) {
        int[] res = new int[200];
        res[0]=1;
        int res_size=1;
        for (int x = 2; x <= num; x++) {
            res_size = multiply(x,res,res_size);
        }
        System.out.println("factorial of num: "+num);
        for (int i = res_size-1; i >=0; i--) {
            System.out.print(res[i]);
        }
    }
    static int multiply (int x, int[] res, int res_size){
        int carry = 0;
        for (int i = 0; i < res_size; i++) {
            int prod = res[i]*x+carry;
            res[i]=prod%10;
            carry=prod/10;
        }
        while(carry!=0){
            res[res_size]=carry%10;
            carry=carry/10;
            res_size++;
        }
        return res_size;

    }
}



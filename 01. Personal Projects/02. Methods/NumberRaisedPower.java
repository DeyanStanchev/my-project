package methods;

import java.util.Scanner;

public class NumberRaisedPower {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double num = Double.parseDouble(scanner.nextLine());
        int power = Integer.parseInt(scanner.nextLine());

        double result = raisedPower(num,power);
        System.out.println(result);


    }
    static double raisedPower (double a, int b){
        return Math.pow(a,b);
    }
}

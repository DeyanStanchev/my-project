package stringManipulationExercises;

public class ReverseASentence {
    public static void main(String[] args) {

        String text = "C# is not C++ and PHP is not Delphi";
        String[] partsArr = text.split(" ");
        String reverseText = reverse(partsArr);
        System.out.println(reverseText);
    }

    private static String reverse(String[] partsArr) {
        StringBuilder sb = new StringBuilder();
        for (int i = partsArr.length-1; i >= 0 ; i--) {
            sb.append(partsArr[i]).append(" ");
        }
        return sb.toString();
    }
}

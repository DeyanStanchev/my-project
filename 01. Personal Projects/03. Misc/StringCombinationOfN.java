package recurssion;

import java.util.Scanner;

public class StringCombinationOfN {
    public static int variations;
    public static String[] inputWords;
    public static String[] sortedWords;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter how many words followed by the words on a new line");
        inputWords = getArray(scanner);
        System.out.print("Number of words in each combination: ");
        variations = scanner.nextInt();
        scanner.close();
        sortedWords = new String[inputWords.length];
        wordsCombinations(0,0);
    }
    public static void wordsCombinations(int currentRow,int number){
        if ( currentRow >= variations){
            printLoops();
            return;
        }
        for (int i = number; i < inputWords.length; i++) {
            sortedWords[currentRow] = inputWords[i];
            wordsCombinations(currentRow+1,i+1);
        }

    }
    public static void printLoops(){
        for (int i = 0; i < variations ; i++) {
            System.out.printf("%s ", sortedWords[i]);
        }
        System.out.println();

    }

    public static String[] getArray(Scanner scanner) {
        int length = Integer.parseInt(scanner.nextLine());
        String[] words = new String[length];
        for (int i = 0; i < words.length; i++) {
            words[i] = scanner.next();
        }
        return words;
    }
}

package Collectionz;

import javax.print.attribute.IntegerSyntax;
import java.util.ArrayList;

public class ArrayListNums {

    public static void main(String[] args) {

        ArrayList list = new ArrayList();
        list.add(2);
        list.add(3);
        list.add(4);
        int sum = 0;
        for (int i = 0; i < list.size(); i++) {
            Integer value = (Integer) list.get(i);
            sum = sum + value.intValue();
        }
        System.out.println("Sum = " + sum);
    }
}

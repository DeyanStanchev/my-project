package numeralSystems;

import java.util.Scanner;

public class BinaryToDecimalHorner {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String binaryNum = scanner.nextLine();

        int decimalNum=0;

        for (int i = binaryNum.length()-1; i >=0 ; i--) {
            char current = binaryNum.charAt(i);
            int currentNum=0;
            switch (current){
                case '1':
                    currentNum = (int)Math.pow(2,(binaryNum.length()-(i+1)));
                    break;
                case'0':
                    break;
            }
            decimalNum += currentNum;
        }
        System.out.println(decimalNum);
    }
}

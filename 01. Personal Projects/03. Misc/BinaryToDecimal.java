package numeralSystems;

import java.util.Scanner;

public class BinaryToDecimal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String binaryNum= scanner.nextLine();

        System.out.println(Integer.parseInt(binaryNum,2));
    }
}

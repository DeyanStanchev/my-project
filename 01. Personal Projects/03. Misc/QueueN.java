package Collectionz;

import java.util.LinkedList;
import java.util.Queue;

public class QueueN {
    public static void main(String[] args) {

        int n = 3;
        int p = 16;

        Queue<Integer> queue = new LinkedList<Integer>();
        queue.offer(n);
        int index = 0;
        System.out.print("S =");
        while(queue.size()>0){
            index ++;
            int current = queue.poll();
            System.out.print(" "+current);
            if(current == p){
                System.out.println();
                System.out.print("Index = " + index);
                return;
            }
            queue.offer(current+1);
            queue.offer(current*2);
        }
    }
}

package objectsAndClassessExercises;

public class TimePastSince1stJanuary1970 {
    public static void main(String[] args) {
        long timePast = System.currentTimeMillis();
        long seconds = (timePast/1000);
        long printSeconds = seconds%60;
        long minutes = (seconds/60)%60;
        long hours = (seconds/3600)%60;
        long days = (seconds/3600)/24;
        System.out.println(seconds);
        System.out.println("Days past: "+days +" Hours past: "+hours+" Minutes past: "+minutes+" Seconds past: "+printSeconds);
    }
}

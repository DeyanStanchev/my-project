package Collectionz;

import java.util.ArrayList;

public class UnionIntersectTwo {

    public static void main(String[] args) {

        ArrayList<Integer> firstList = new ArrayList<>();
        firstList.add(1);
        firstList.add(2);
        firstList.add(3);
        firstList.add(4);
        firstList.add(5);
        System.out.print("First list= ");
        printList(firstList);


        ArrayList<Integer> secondList = new ArrayList<>();
        secondList.add(2);
        secondList.add(4);
        secondList.add(6);
        System.out.print("Second list= ");
        printList(secondList);


        ArrayList<Integer> unionList = new ArrayList<>();
        unionList.addAll(firstList);
        unionList.removeAll(secondList);
        unionList.addAll(secondList);
        System.out.print("Union = ");
        printList(unionList);

        ArrayList<Integer> intersectList = new ArrayList<>();
        intersectList.addAll(firstList);
        intersectList.retainAll(secondList);
        System.out.print("Intersect = ");
        printList(intersectList);
    }
    public static void  printList (ArrayList<Integer> list){
        System.out.print("{ ");
        for (Integer item:list) {
            System.out.print(item);
            System.out.print(" ");
        }
        System.out.println("}");
    }
}

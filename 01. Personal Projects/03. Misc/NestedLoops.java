package recurssion;


import java.util.Scanner;

public class NestedLoops {
    public static int numberOfLoops;
    public static int numberOfIterations;
    public static int[] loops;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("N = ");
        numberOfLoops = scanner.nextInt();

        System.out.print("K = ");
        numberOfIterations = scanner.nextInt();

        scanner.close();

        loops = new int[numberOfLoops];
        nestedLoops(0);
    }
    public static void nestedLoops(int currentLoop) {
        if (currentLoop == numberOfLoops) {
            printLoops();
            return;
        }
        for (int i = 1; i <= numberOfIterations; i++) {
            loops[currentLoop] = i;
            nestedLoops(currentLoop + 1);
        }
    }

    public static void printLoops() {
        for (int i = 0; i < numberOfLoops; i++) {
            System.out.printf("%d ", loops[i]);
        }
        System.out.println();
    }
}


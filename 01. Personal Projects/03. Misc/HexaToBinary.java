package numeralSystems;

import java.util.Scanner;

public class HexaToBinary {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String hexNum = scanner.nextLine();

        String value = "";

        String currentDigit = "";

        for (int i = 0; i <hexNum.length(); i++) {
            char current = hexNum.charAt(i);
            switch (current) {
                case '0':
                    currentDigit = "0000";
                    break;
                case '1':
                    currentDigit = "0001";
                    break;
                case '2':
                    currentDigit = "0010";
                    break;
                case '3':
                    currentDigit = "0011";
                    break;
                case '4':
                    currentDigit = "0100";
                    break;
                case '5':
                    currentDigit = "0101";
                    break;
                case '6':
                    currentDigit = "0110";
                    break;
                case '7':
                    currentDigit = "0111";
                    break;
                case '8':
                    currentDigit = "1000";
                    break;
                case '9':
                    currentDigit = "1001";
                    break;
                case 'a':
                    currentDigit = "1010";
                    break;
                case 'b':
                    currentDigit = "1011";
                    break;
                case 'c':
                    currentDigit = "1100";
                    break;
                case 'd':
                    currentDigit = "1101";
                    break;
                case 'e':
                    currentDigit = "1110";
                    break;
                case 'f':
                    currentDigit = "1111";
                    break;

            }

            value += currentDigit;
        }
        System.out.printf("%s",value);
    }
}

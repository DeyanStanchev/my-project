package Collectionz;

import java.util.ArrayList;
import java.util.Date;

public class ArrayListTest {

    public static void main(String[] args) {

        ArrayList list = new ArrayList();
        list.add("Hello");
        list.add(5);
        list.add(3.14159);
        list.add(new Date());

        for (int i = 0; i < list.size(); i++) {
            Object value = list.get(i);
            System.out.printf("Index=%d; Value = %s%n",i,value);
        }
    }
}

package numeralSystems;

import java.util.Scanner;

public class RomeToDecimal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String romanNum = scanner.nextLine();

        int decimalNum = 0;
        char nextSymbol = 'a';


        for (int i = 0; i <romanNum.length(); i++) {
            int sum = 0;
            char current = romanNum.charAt(i);
            if (i < romanNum.length() - 1) {
                nextSymbol = romanNum.charAt(i + 1);
            }
            int currentDec = 0;
            int nextDec = 0;
            switch (current) {
                case 'I':
                    currentDec = 1;
                    break;
                case 'V':
                    currentDec = 5;
                    break;
                case 'X':
                    currentDec = 10;
                    break;
                case 'L':
                    currentDec = 50;
                    break;
                case 'C':
                    currentDec = 100;
                    break;
                case 'D':
                    currentDec = 500;
                    break;
                case 'M':
                    currentDec = 1000;
                    break;
            }
            switch (nextSymbol) {
                case 'I':
                    nextDec = 1;
                    break;
                case 'V':
                    nextDec = 5;
                    break;
                case 'X':
                    nextDec = 10;
                    break;
                case 'L':
                    nextDec = 50;
                    break;
                case 'C':
                    nextDec = 100;
                    break;
                case 'D':
                    nextDec = 500;
                    break;
                case 'M':
                    nextDec = 1000;
                    break;
            }

            if (currentDec >= nextDec ) {
                sum += currentDec ;
            }  else {
                sum -= currentDec;
            }
            decimalNum += sum;
            current = 'a';
            nextSymbol = 'a';
        }
        System.out.println(decimalNum);
    }
}

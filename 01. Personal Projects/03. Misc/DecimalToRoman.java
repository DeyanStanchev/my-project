package numeralSystems;

import java.util.Scanner;

public class DecimalToRoman {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int decNum = Integer.parseInt(scanner.nextLine());

        String romanNum = Integer.toString(decNum);
        int romanLength = romanNum.length();
        String roman = "";

        for (int i = 0; i < romanNum.length(); i++) {
            if ( decNum / 1000 != 0) {
                int digit = decNum / 1000;
                while (digit > 0) {
                    roman += "M";
                    digit--;
                }
                decNum %= 1000;
                continue;
            }
            if ( decNum / 100 != 0) {
                int digit = decNum / 100;
                switch (digit) {
                    case 1:
                        roman += "C";
                        break;
                    case 2:
                        roman += "CC";
                        break;
                    case 3:
                        roman += "CCC";
                        break;
                    case 4:
                        roman += "CD";
                        break;
                    case 5:
                        roman += "D";
                        break;
                    case 6:
                        roman += "DC";
                        break;
                    case 7:
                        roman += "DCC";
                        break;
                    case 8:
                        roman += "DCC";
                        break;
                    case 9:
                        roman += "CM";
                        break;
                }
                decNum %= 100;
                continue;
            }
            if (decNum / 10 != 0) {
                int digit = decNum / 10;
                switch (digit) {
                    case 1:
                        roman += "X";
                        break;
                    case 2:
                        roman += "XX";
                        break;
                    case 3:
                        roman += "XXX";
                        break;
                    case 4:
                        roman += "XL";
                        break;
                    case 5:
                        roman += "L";
                        break;
                    case 6:
                        roman += "LX";
                        break;
                    case 7:
                        roman += "LXX";
                        break;
                    case 8:
                        roman += "LXXX";
                        break;
                    case 9:
                        roman += "XC";
                        break;
                }
                decNum %= 10;
                continue;
            }
            if ( decNum!=0) {
                switch (decNum) {
                    case 1:
                        roman += "I";
                        break;
                    case 2:
                        roman += "II";
                        break;
                    case 3:
                        roman += "III";
                        break;
                    case 4:
                        roman += "IV";
                        break;
                    case 5:
                        roman += "V";
                        break;
                    case 6:
                        roman += "VI";
                        break;
                    case 7:
                        roman += "VII";
                        break;
                    case 8:
                        roman += "VIII";
                        break;
                    case 9:
                        roman += "IX";
                        break;
                }
            }
        }
        System.out.printf("%s", roman);
    }
}

package numeralSystems;

import java.util.Scanner;

public class BinaryToHexadecimal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String binaryNum = scanner.nextLine();

        int decNum = Integer.parseInt(binaryNum,2);
        System.out.println(Integer.toString(decNum,16));
    }
}
